(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["YL"] = factory();
	else
		root["YL"] = factory();
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "MFGrid", function() { return /* binding */ MFGrid_MFGrid; });

// CONCATENATED MODULE: ./src/js/event/event.js
let EventType={
	"ALL":"ALL",  //表示监听所有事件
	"NONE":"NONE",//空类型，该类型的事件不会转发任何监听者
	"INTRACTIVE":"INTRACTIVE", //交互事件
	"COORDINATECHANGE":"COORDINATECHANGE", //坐标系统改变（所有的视图改变都由该事件触发。）

	"OUTPUT":"OUTPUT" ,//控件数据输出事件
	"DATAREFRESH":"DATAREFRESH", // 数据源刷新事件

	"POINTCSS":"POINTCSS", //鼠标样式事件
};





// CONCATENATED MODULE: ./src/js/util.js

let uuid= function () {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
        d += performance.now(); //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}


let sumOffset=function(ele){
        

    let parentEle=ele.offsetParent;
    let offsetLeft=ele.offsetLeft;
    let offsetTop=ele.offsetTop;
    while(true){

        //如果父偏移对象不存在，则返回
        if(!parentEle){
            return [offsetLeft,offsetTop];
        }
        //对偏移量累加
        offsetLeft+=parentEle.offsetLeft;
        offsetTop+=parentEle.offsetTop;
        
       parentEle=parentEle.offsetParent;
    }

   

}


let loadFile=function(filename, fileType){
    if(fileType == "js"){
        var oJs = document.createElement('script');
        oJs.setAttribute("type","text/javascript");
        oJs.setAttribute("src", filename);//文件的地址 ,可为bai绝对及相对路径du
        document.getElementsByTagName("head")[0].appendChild(oJs);//绑定
    }else if(fileType == "css"){
        var oCss = document.createElement("link");
        oCss.setAttribute("rel", "stylesheet");
        oCss.setAttribute("type", "text/css");
        oCss.setAttribute("href", filename);
        document.getElementsByTagName("head")[0].appendChild(oCss);//绑定
    }
}


let loadStyle=function(cssString){
    var doc=document;
    var style=doc.createElement("style");
    style.setAttribute("type", "text/css");
 
    if(style.styleSheet){// IE
        style.styleSheet.cssText = cssString;
    } else {// w3c
        var cssText = doc.createTextNode(cssString);
        style.appendChild(cssText);
    }
 
    var heads = doc.getElementsByTagName("head");
    if(heads.length)
        heads[0].appendChild(style);
    else
        doc.documentElement.appendChild(style);
}



// CONCATENATED MODULE: ./src/js/event/EventCenter.js



class EventCenter_EventCenter{
	

	constructor(){
		this.listeners=[];
		this.eventListenerMap={}; //{ "eventType1":[listener1,listener2]}
	}

	/*在事件中心注册客户端
	obj-注册客户端对象
	eventTypes-事件类型 数组[]
	call-事件的处理方法
	*/
	regist(obj,eventTypes,call){
		let listener=new EventCenter_Lisenter(obj,eventTypes,call);
		this.listeners.push(listener);

		if(this.checkIfEventTypesValiable(eventTypes)){
			
			if(EventType.ALL in eventTypes && eventTypes.length!=1){
				throw "监听器 EventType.ALL 类型不能与其他类型一起使用";
				return ;
			}

			for(let i=0;i<eventTypes.length;i++){
				let eventType=eventTypes[i];

				if(eventType in this.eventListenerMap){
					this.eventListenerMap[eventType].push(listener);
				}else{
					this.eventListenerMap[eventType]=[listener];
				}

			}

		}

		

	}

	/*发送事件消息
	event-事件，对象-js/event/EventCenter.js-Event
	*/
	postEvent(event){
		//检查是否在
		if(!this.checkIfRegistered(event.senderObj)){
			throw "对象未在 事件中心注册，不允许发送消息,"+event.senderObj.constructor.name;
			return;
		}

		//循环调用事件的处理逻辑
		let eventType=event.type;

		let listnerArray=this.getListenersByEventType(eventType);
		for(let i=0;i<listnerArray.length;i++){
			listnerArray[i].call(event);
		}

		//对all类型通知
		 eventType=EventType.ALL;
		 listnerArray=this.getListenersByEventType(eventType);
		for(let i=0;i<listnerArray.length;i++){
			listnerArray[i].call(event);
		}
		
	}

	/*检查发送者是否在事件中心注册过
	  只有注册过的对象才能处理接收和发送事件
	  senderObj-消息源对象
	*/
	checkIfRegistered(senderObj){
		for(let i=0;i<this.listeners.length;i++){
			if(senderObj.eventCenterId==this.listeners[i].getId()){
				return true;
			}
		}
		return false;
		
	}

	/*检查事件类型是否符合要求
	eventTypes-事件类型数组（字符串的数组）
	return- true-符合要求 false-不符合要求
	*/
	checkIfEventTypesValiable(eventTypes){
		for(let i=0;i<eventTypes.length;i++){
			if(! (eventTypes[i] in EventType)){
				return false;
			}
		}
		return true;
	}


	/*根据事件类型获取监听者
	eventType-事件类型
	return-Listener数组
	*/
	getListenersByEventType(eventType){
		if(eventType in this.eventListenerMap){
			let listnerArray=this.eventListenerMap[eventType]
			return listnerArray;
		}else{
			return [];
		}
	}


}

//----------------将事件中心的某些方法设置成全局

/*事件中心的获取*/
EventCenter_EventCenter.instance=function(){
	
	if("eventCenter" in window){
		return window.eventCenter;
	}else{
		throw "EventCenter 对象未初始化"
	}
	
}

/*事件中心绑定到window对象，使其成为全局对象*/
EventCenter_EventCenter.preInit=function(eventCenter){
	if("eventCenter" in window){
		//throw "EventCenter 不需要重复初始化" ;
		return;
	}
	 window.eventCenter=eventCenter;
}


EventCenter_EventCenter.regist=function(obj,eventTypes,call){
	EventCenter_EventCenter.instance().regist(obj,eventTypes,call);
}

EventCenter_EventCenter.postEvent=function(event){
	EventCenter_EventCenter.instance().postEvent(event);
}


class EventCenter_Lisenter{

	/*构造函数
	obj-注册者
	eventTypes-监听的事件类型[type1,type2]
	call-监听者对象*/

	constructor(obj,evenTypes,call){
		this._obj=obj;
		this._types=evenTypes;
		this._id=uuid();
		this._obj.eventCenterId=this._id;
		
		//监听对象call 如果为空则，将obj设置为监听者，监听者必须实现listenerCall 方法
		if(!call){
			this._call=obj;
		}else{
			this._call=call;	
		}
		

	}

	getId(){
		return this._id;
	}
	call(event){
		this._call.listenerCall(event);
	}

}

class Event{
	constructor(senderObj,eventType,eventData){
		this.senderObj=senderObj;
		this.eventType=eventType;
		this.eventData=eventData;
	}
}


// CONCATENATED MODULE: ./src/js/intractive/Movable.js






class Movable_Movable{

	constructor(intractiveLayer){
		let mode=intractiveLayer.operationMode;

		this.intractiveLayer=intractiveLayer;


		//鼠标状态 1-按下 0-弹起 
		this._mouseUpStatus=0;
		//鼠标移入状态1-移入  0-移出
		this._mouseInStauts=0;

		//移动状态 0-非移动  1-移动。在鼠标按下时重置为未移动，鼠标移动事件时设为 移动状态
		this._movingStatus=0;
		EventCenter_EventCenter.regist(this,[EventType.NONE]);
	}


   /* 鼠标点击逻辑
	mouseEvent-鼠标事件
   */
    onmousedown(mouseEvent){
    	this._movingStatus=0
    	this._mouseUpStatus=1;
    	

    	

    	let offsetXY=this.intractiveLayer.getViewOffset();
    	//let x=mouseEvent.clientX-offsetXY[0];
    	//let y=mouseEvent.clientY-offsetXY[1];
    	
    	let x=mouseEvent.offsetX;
    	let y=mouseEvent.offsetY;
    	this._mouseLastDragPosition=[x,y];
    	this._mouseDownPosition=[x,y];
    }

	/* 鼠标点击放开逻辑
	mouseEvent-鼠标事件
   */
    onmouseup(mouseEvent){
    	this._mouseUpStatus=0;
    }


    onmouseover(mouseEvent){
    	this._mouseInStauts=1;
    }

    onmouseout(mouseEvent){
    	this._mouseInStauts=0;
    	this._mouseUpStatus=0; //移出视图 ，也当做松开鼠标键
    }

    /*鼠标单击*/
     onclick(mouseEvent){
    	//如果鼠标处于移动事件，则不触发点击事件
    	if(this._movingStatus==1){
    		return
    	}
    	let event=new Event();
    	let that=this;
    	event.senderObj=this;
		event.type=EventType.INTRACTIVE;

    	event.eventData={
				"action":"click", //单击
				"startPosition":that._mouseDownPosition			//点击起始点
			};
		EventCenter_EventCenter.postEvent(event);
    }

	/* 鼠标移动逻辑
	mouseEvent-鼠标事件
	return
   */
    onmousemove(mouseEvent){
    	this._movingStatus=1
    	//console.log("moving")
    	if(this._mouseUpStatus==1&&this._mouseInStauts==1){
	    		//鼠标按下并移动--拖动操作


	    	let event=new Event();
	    	event.senderObj=this;
			event.type=EventType.INTRACTIVE;

			let that=this;

			let x=mouseEvent.offsetX;
    	    let y=mouseEvent.offsetY;

			//let moveDistance=[mouseEvent.clientX-that._mouseLastDragPosition[0],mouseEvent.clientY-that._mouseLastDragPosition[1]]
			let moveDistance=[x-that._mouseLastDragPosition[0],y-that._mouseLastDragPosition[1]]


			if(moveDistance[0]==0&&moveDistance[1]==0){
				//如果未移动
				return;
			}
			
			event.eventData={
				"action":"drag", //动作，拖拽
				"startPosition":that._mouseDownPosition, 				//拖拽起始点
				"movePosition":[x,y], //拖拽到达点
				"lastPosition":that._mouseLastDragPosition , 			//鼠标上次移动的位置
				"moveDistance":moveDistance
			};

	    	EventCenter_EventCenter.postEvent(event);

	    	//更新上一次鼠标拖动位置为当前位置
	    	this._mouseLastDragPosition=[x,y];
    	}else{
    		//鼠标弹起-鼠标移动操作
    		//doNothing
    	}
    }


}


// CONCATENATED MODULE: ./src/js/intractive/Scalable.js





class Scalable_Scalable{
	constructor(intractiveLayer){
		
		this.currentPosition;
		this.intractiveLayer=intractiveLayer;

		EventCenter_EventCenter.regist(this,[EventType.NONE]);
	}


	onmousewheel(mouseEvent){
		//刷新鼠标位置
		this.refreshCurrentMousePointerPosition(mouseEvent)

        mouseEvent.preventDefault();
        var e = window.mouseEvent || mouseEvent; // old IE support
        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
        //  alert(delta+"->"+ e.wheelDelta+'->'+ e.detail);

       
         let event=new Event();
    	 event.senderObj=this;
		 event.type=EventType.INTRACTIVE;

		 event.eventData={
			"action":"scale", 							//动作，拖拽
			"startPosition":this.currentPosition, 	    //拖拽起始点
			"scaleSizedelta": delta  					//缩放变化大小
		 };
		 EventCenter_EventCenter.postEvent(event);
    }
    
    onmousemove(mouseEvent){
    		this.refreshCurrentMousePointerPosition(mouseEvent)
			

	}

	refreshCurrentMousePointerPosition(mouseEvent){
	    	let x=mouseEvent.offsetX;
    	    let y=mouseEvent.offsetY;

			this.currentPosition=[x,y];//当前鼠标移动到的位置
	}

}


// CONCATENATED MODULE: ./src/js/intractive/Selectable.js






class Selectable_Selectable{

	constructor(intractiveLayer){

		//鼠标状态 1-按下 0-弹起 
		this._mouseUpStatus=0;
		//鼠标移入状态1-移入  0-移出
		this._mouseInStauts=0;

		this.intractiveLayer=intractiveLayer;
		EventCenter_EventCenter.regist(this,[EventType.NONE]);

		//移动状态 0-非移动  1-移动
		this._movingStatus=0;


	}

	
	/* 鼠标点击逻辑
	mouseEvent-鼠标事件
   */
    onmousedown(mouseEvent){
    	this._movingStatus=0;
    	//开始选择
    	this._mouseUpStatus=1;

    	let offsetXY=this.intractiveLayer.getViewOffset();
    	//let x=mouseEvent.clientX-offsetXY[0];
    	//let y=mouseEvent.clientY-offsetXY[1];

    	let x=mouseEvent.offsetX;
    	let y=mouseEvent.offsetY;
    	
    	//鼠标按下位置
    	this._mouseDownPosition=[x,y];
    	//鼠标上次移动到的位置
    	this._mouseLastDragPosition=[x,y];
    }

	/* 鼠标点击放开逻辑
	mouseEvent-鼠标事件
   */
    onmouseup(mouseEvent){
    	//选择完成
    	this._mouseUpStatus=0;
    	

    }


    onmouseover(mouseEvent){
    	this._mouseInStauts=1;
    }

    onmouseout(mouseEvent){
    	this._mouseInStauts=0;
    	this.onmouseup(mouseEvent); //移出视图 ，也当做选择完成
    }

    onclick(mouseEvent){
    	//如果鼠标处于移动事件，则不触发点击事件
    	if(this._movingStatus==1){
    		return
    	}
    	let event=new Event();
    	let that=this;
    	event.senderObj=this;
		event.type=EventType.INTRACTIVE;

    	event.eventData={
				"action":"click", //单击
				"startPosition":that._mouseDownPosition			//拖拽起始点
			};
		EventCenter_EventCenter.postEvent(event);
    }

	/* 鼠标移动逻辑
	mouseEvent-鼠标事件
	return
   */
    onmousemove(mouseEvent){
    	if(this._mouseUpStatus==1&&this._mouseInStauts==1){
	    		//鼠标按下并移动--拖动操作
	    	this._movingStatus=1;	

	    	let event=new Event();
	    	event.senderObj=this;
			event.type=EventType.INTRACTIVE;

			let that=this;


			//clientX 和clientY 要减去屏幕偏移位置 offsetTop,
			let offsetXY=this.intractiveLayer.getViewOffset();
	    	//let x=mouseEvent.clientX-offsetXY[0];
	    	//let y=mouseEvent.clientY-offsetXY[1];

	    	let x=mouseEvent.offsetX;
    	    let y=mouseEvent.offsetY;

			let currentPosition=[x,y];//当前鼠标移动到的位置

			let moveDistance=[currentPosition[0]-that._mouseLastDragPosition[0],currentPosition[1]-that._mouseLastDragPosition[1]]
			
			event.eventData={
				"action":"drag", //动作，拖拽
				"startPosition":that._mouseDownPosition, 				//拖拽起始点
				"movePosition":currentPosition, 						//拖拽到达点
				"lastPosition":that._mouseLastDragPosition , 			//鼠标上次移动的位置
				"moveDistance":moveDistance
			};

	    	EventCenter_EventCenter.postEvent(event);

	    	//更新上一次鼠标拖动位置为当前位置
	    	this._mouseLastDragPosition=[mouseEvent.clientX,mouseEvent.clientY];
    	}else{
    		//鼠标弹起-鼠标移动操作
    		//doNothing
    	}
    }


}


// CONCATENATED MODULE: ./src/js/intractive/ModeChangePan.js







/*模式切换控制面板*/
class ModeChangePan_ModeChangePan{

	constructor(intractiveLayer){

		

		this.intractiveLayer=intractiveLayer;
		this.mode=intractiveLayer.operationMode;
		
		this._initStyle();
		this._initPan();
		
		//需要对模式进行设置.移动模式
		intractiveLayer.operationMode=OperationMode.MOVEMODE;
	}


	_initPan(){


		let that=this;
		
		//加载面板元素
		let pandiv=document.createElement("div");
		pandiv.style="float:right;height:30px;margin-top:10px;margin-right:10px;";
		

		 pandiv.innerHTML=
		   "<div class='toggle-button-cover' style='border:1px'>\
		      <div class='button-cover'>\
		        <div class='button b2' id='button-16'>\
		          <input type='checkbox' class='checkbox'>\
		          <div class='knobs'></div>\
		          <div class='layer'></div>\
		        </div>\
		      </div>";

		 this.intractiveLayer._viewEle.appendChild(pandiv);

		 let button= document.getElementById("button-16");
		 button.onclick=function(e){
		 	 //e.preventDefault();
		 	 if(OperationMode.MOVEMODE==that.intractiveLayer.operationMode){
		 	 	that.intractiveLayer.operationMode=OperationMode.SELECTMODE;
		 	 }else{
		 	 	that.intractiveLayer.operationMode=OperationMode.MOVEMODE;
		 	 }
		 	 //阻止冒泡，将事件传递到父组件
       		e.stopPropagation();
		 }
		

	}

	_initStyle(){
		let cssStr="*{    user-select: none;    -webkit-tap-highlight-color:transparent;}*:focus{    outline: none;}body{    font-family: Arial, Helvetica, sans-serif;    margin: 0;    background-color: #f1f9f9;}#app-cover{    display: table;    width: 600px;    margin: 80px auto;    counter-reset: button-counter;}.row{    display: table-row;}.toggle-button-cover{    display: table-cell;    position: relative;    width: 75px;    height: 30px;    box-sizing: border-box;}.button-cover{    height: 30px;    margin: 0px;    background-color: #fff;    box-shadow: 0 10px 20px -8px #c5d6d6;    border-radius: 4px;}.button-cover:before{    counter-increment: button-counter;    content: counter(button-counter);    position: absolute;    right: 0;    bottom: 0;    color: #d7e3e3;    font-size: 12px;    line-height: 1;    padding: 5px;}.button-cover, .knobs, .layer{    position: absolute;    top: 0;    right: 0;    bottom: 0;    left: 0;}.button{    position: relative;    top: 50%;    width: 74px;    height: 30px;    margin: -15px auto 0 auto;    overflow: hidden;}.button.r, .button.r .layer{    border-radius: 100px;}.button.b2{    border-radius: 2px;}.checkbox{    position: relative;    width: 100%;    height: 100%;    padding: 0;    margin: 0;    opacity: 0;    cursor: pointer;    z-index: 3;}.knobs{    z-index: 2;}.layer{    width: 100%;    background-color: #ebf7fc;    transition: 0.3s ease all;    z-index: 1;}/* Button 16 */#button-16 .knobs:before{    content: '拖动';    position: absolute;    top: 1px;    left: 4px;    width: 40px;    height: 20px;    color: #fff;    font-size: 10px;    font-weight: bold;    text-align: center;    line-height: 1;    padding: 9px 2px;    background-color: #03A9F4;    border-radius: 2px;    transition: 0.3s ease all, left 0.3s cubic-bezier(0.18, 0.89, 0.35, 1.15);    }#button-16 .checkbox:active + .knobs:before{    width: 46px;}#button-16 .checkbox:checked:active + .knobs:before{    margin-left: -26px;}#button-16 .checkbox:checked + .knobs:before{    content: '选择';    left: 30px;    background-color: #F44336;}#button-16 .checkbox:checked ~ .layer{    background-color: #fcebeb;}";
		loadStyle(cssStr);
	}
  
	

}


// CONCATENATED MODULE: ./src/js/intractive/QuickLocationPan.js







/*模式切换控制面板*/
class QuickLocationPan_QuickLocationPan{

	constructor(intractiveLayer){

		

		this.intractiveLayer=intractiveLayer;
		this.mode=intractiveLayer.operationMode;
	
		
		this._initStyle();
		this._initPan();
			//事件中心注册
		EventCenter_EventCenter.regist(this,[EventType.NONE],null);
	}


	_initPan(){
		let that=this;
		
		//加载面板元素
		let pandiv=document.createElement("div");
		pandiv.style="float:left;height:30px;margin-top:10px;margin-left:10px;border:1px solid #808080;";
		

		//<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAWbSURBVHjapFZvaBtlGP/d5ZJr7po2Tdqwy7qYdFnXHk3GVkHZB5WVdeyTaP27YmVOdDJEYTphyvzzccoEP/ptwvw3Vv9Ct8ksdNmko2vBtHUpmyGjoUmapM1yd7k/uff8spRsiQr6wAP3vs9z93t+z/s8z3vU/Pw8OI5DOp3G0aNHYRgGAoFAJJ1OP7G8vPxYsVgMm6bpbGlpybvd7oW+vr6fVFW9KIriitvtRm9vL3ieByEEfycMAJimCZ7nwfO8Z2lp6YOFhYXXdF1n6h1lWe6UZbkvnU6PeDyem21tbe8JgvANRVH4N6FZlkUwGMStW7cCN2/ePJ9Op1+/H+B+KRaL4YmJia+vXLnyqcfj+UcWAGDbs2cPLly4IJw8efJ8MpnceY/RZkNXV1fC6XQmATgMw+Dr7clk8mGGYRy7d+/+VVVV0DTdVJlisYjTp0+/s7S0NFB7maIohMPhcZfL9Ukmk0nSNK273W5XKBR6dHZ29t1CodBb8z1z5sxxp9MZC4fDE3fu3EGz9NGZTGYgHo+/Ub/pcrk+pCjqGZfL9ZumaRnDMIo0Tad4nv8iEokM9/f3z9E0veE/PT19pFKp2DRNg6IoDcpMTU2N1Od0cHDw+7a2tg8oioIkSRuRWZaFSqWCSqWS2rdv34uFQuFyLpdrB4BEIrE3k8ns3L59+4wkSY1M5ubmhjcWNG3t3bv3o7GxMQwPD0NRFFiWBcuyoGkaWJYFAKRSqbggCN/W2Oi67sjn8zur1So0TWtQJp/P99RA3G53YnJyMhmLxWAYBhRFAcuyIISgp6cHLMuiq6sLqqqiu7t7YnFx8RAhhAaAxcXF3nK5DFVVG/uEENJSW9jt9mI2myW6rm+kKRQKYdOmTeA4DqZpoq44SgAIAPpupbHpdLppOTMOh2NN13U3AEiSFDpy5IgjEAhAkiSMj4/DMAxwHIcbN26gpaUFHo8HDocDuVwuTAjZ6Kddu3atiaIIWZYbQXw+3/zy8nLoblcL+Xx+D8dx3yqKAqfTCVVVQQjB2toaJEnC4cOHMTAwgOPHjz9Tz2zz5s1/iKKI9fX1xoMfGhoar984e/bsx7FYTLh27RqcTifqigIA4PF4MD8/fzCVSj1Ss4VCobmtW7deLJVKqFarG2qaJgzDABOJRH72er1/FgqFHgBYWVkJTE9PTwwNDY3qur5AURRomgZFUfD5fJicnDx86dKlzzRNs9dAfD7fqiRJxUQicQ8DXdfhcrlgO3HihOJyuVZjsdhIzShJ0qbbt28/tba2JnZ2djpZln2gUCjsX11dfX9mZuZNTdNs9R/L5XJBVVV7W1tbz5fLZaNSqUBVVayvr4NhGFBTU1Ow2Ww4derUyXPnzr19fz7tdjsBQAghTP0ZNJPBwcGvuru7DyqKogGAoijwer2wjY6OQpZldHR0/FIoFFpKpdJDuq5vREoIoQghtGVZ/zrSV1ZWIhzHiV6v9wfDMMxqtQqO42AbGxtDtVpFKpVCa2vrJVmWp1mWba9Wq0HDMBpGfiAQ+F0QhBv5fD7YDCiTyfTzPB/p7Oz8TpZlk+M4MPXVc3eMXAyFQhd5nn/Q4/HsyGaz4Ww26xBFsShJUqK9vf1yMBgs2+32z+Px+GgzoNnZ2cdpmv6yo6PjBQBKQ6SWZaFcLoNhmJlwODxD0zTK5TK2bduGbDaLTCaDUqmELVu2vAyAjsfjzzcDun79+pOiKOrRaPRQ0xuQoigQQqAoCjRNg2maG8+1cVOpVFS/3/8KRVF0PB5/9v4zsywLCwsLz7W3t5do/EexLAuqqkp+v/+lHTt2jP+d39WrV1/9zyB1PxiKIAgHotHoj03vd5st+b9BKIqCoiiaIAjPDgwMNDA6duzYW/8bpAakqqrq9/sPRKPRzwHccTgc8yMjI0/v379//K8BAAT30BUo/WZ0AAAAAElFTkSuQmCC' alt=''>\
			   
		//
		//图片使用base64转码
		 pandiv.innerHTML=
		   "<div class='search' id='mfgrid:quicklocationpan:search'>\
			  <input type='text' name='condition' placeholder='输入坐标' id='mfgrid:quicklocationpan:input'>\
			  <input type='submit' value='搜索' id='mfgrid:quicklocationpan:button'\
			     type='background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAWbSURBVHjapFZvaBtlGP/d5ZJr7po2Tdqwy7qYdFnXHk3GVkHZB5WVdeyTaP27YmVOdDJEYTphyvzzccoEP/ptwvw3Vv9Ct8ksdNmko2vBtHUpmyGjoUmapM1yd7k/uff8spRsiQr6wAP3vs9z93t+z/s8z3vU/Pw8OI5DOp3G0aNHYRgGAoFAJJ1OP7G8vPxYsVgMm6bpbGlpybvd7oW+vr6fVFW9KIriitvtRm9vL3ieByEEfycMAJimCZ7nwfO8Z2lp6YOFhYXXdF1n6h1lWe6UZbkvnU6PeDyem21tbe8JgvANRVH4N6FZlkUwGMStW7cCN2/ePJ9Op1+/H+B+KRaL4YmJia+vXLnyqcfj+UcWAGDbs2cPLly4IJw8efJ8MpnceY/RZkNXV1fC6XQmATgMw+Dr7clk8mGGYRy7d+/+VVVV0DTdVJlisYjTp0+/s7S0NFB7maIohMPhcZfL9Ukmk0nSNK273W5XKBR6dHZ29t1CodBb8z1z5sxxp9MZC4fDE3fu3EGz9NGZTGYgHo+/Ub/pcrk+pCjqGZfL9ZumaRnDMIo0Tad4nv8iEokM9/f3z9E0veE/PT19pFKp2DRNg6IoDcpMTU2N1Od0cHDw+7a2tg8oioIkSRuRWZaFSqWCSqWS2rdv34uFQuFyLpdrB4BEIrE3k8ns3L59+4wkSY1M5ubmhjcWNG3t3bv3o7GxMQwPD0NRFFiWBcuyoGkaWJYFAKRSqbggCN/W2Oi67sjn8zur1So0TWtQJp/P99RA3G53YnJyMhmLxWAYBhRFAcuyIISgp6cHLMuiq6sLqqqiu7t7YnFx8RAhhAaAxcXF3nK5DFVVG/uEENJSW9jt9mI2myW6rm+kKRQKYdOmTeA4DqZpoq44SgAIAPpupbHpdLppOTMOh2NN13U3AEiSFDpy5IgjEAhAkiSMj4/DMAxwHIcbN26gpaUFHo8HDocDuVwuTAjZ6Kddu3atiaIIWZYbQXw+3/zy8nLoblcL+Xx+D8dx3yqKAqfTCVVVQQjB2toaJEnC4cOHMTAwgOPHjz9Tz2zz5s1/iKKI9fX1xoMfGhoar984e/bsx7FYTLh27RqcTifqigIA4PF4MD8/fzCVSj1Ss4VCobmtW7deLJVKqFarG2qaJgzDABOJRH72er1/FgqFHgBYWVkJTE9PTwwNDY3qur5AURRomgZFUfD5fJicnDx86dKlzzRNs9dAfD7fqiRJxUQicQ8DXdfhcrlgO3HihOJyuVZjsdhIzShJ0qbbt28/tba2JnZ2djpZln2gUCjsX11dfX9mZuZNTdNs9R/L5XJBVVV7W1tbz5fLZaNSqUBVVayvr4NhGFBTU1Ow2Ww4derUyXPnzr19fz7tdjsBQAghTP0ZNJPBwcGvuru7DyqKogGAoijwer2wjY6OQpZldHR0/FIoFFpKpdJDuq5vREoIoQghtGVZ/zrSV1ZWIhzHiV6v9wfDMMxqtQqO42AbGxtDtVpFKpVCa2vrJVmWp1mWba9Wq0HDMBpGfiAQ+F0QhBv5fD7YDCiTyfTzPB/p7Oz8TpZlk+M4MPXVc3eMXAyFQhd5nn/Q4/HsyGaz4Ww26xBFsShJUqK9vf1yMBgs2+32z+Px+GgzoNnZ2cdpmv6yo6PjBQBKQ6SWZaFcLoNhmJlwODxD0zTK5TK2bduGbDaLTCaDUqmELVu2vAyAjsfjzzcDun79+pOiKOrRaPRQ0xuQoigQQqAoCjRNg2maG8+1cVOpVFS/3/8KRVF0PB5/9v4zsywLCwsLz7W3t5do/EexLAuqqkp+v/+lHTt2jP+d39WrV1/9zyB1PxiKIAgHotHoj03vd5st+b9BKIqCoiiaIAjPDgwMNDA6duzYW/8bpAakqqrq9/sPRKPRzwHccTgc8yMjI0/v379//K8BAAT30BUo/WZ0AAAAAElFTkSuQmCC) no-repeat center;   \
         			;cursor:pointer';  \
			   >\
			 </div>";

		 this.intractiveLayer._viewEle.appendChild(pandiv);

		 let div=document.getElementById("mfgrid:quicklocationpan:search");
		 let button= document.getElementById("mfgrid:quicklocationpan:button");
		 let input= document.getElementById("mfgrid:quicklocationpan:input");
		 
		 div.onmousemove=function(e){
		 	//阻止冒泡事件传递到父控件
       		e.stopPropagation();
		 }

		 div.onclick=function(e){
		 	//阻止冒泡事件传递到父控件
       		e.stopPropagation();
		 }

		 button.onclick=function(e){

		 	let locationStr=input.value;
		 	if(!locationStr){
		 		return;
		 	}
		 	 console.log(locationStr)
		 	 //e.preventDefault();

			//阻止冒泡事件传递到父控件
       		e.stopPropagation();


			
		 	 if(locationStr.length>0){
		 	 	let strs=new Array();
		 	 	if(locationStr.indexOf(",")!=-1){
		 	 		strs = locationStr.split(","); //字符分割
		 	 	}else if(locationStr.indexOf("-")!=-1){
		 	 		strs = locationStr.split("-"); //字符分割
		 	 	}else if(locationStr.indexOf("_")!=-1){
		 	 		strs = locationStr.split("_"); //字符分割
		 	 	}

		 	 	if(strs.length==2){
		 	 		let x= parseInt(strs[0]);
		 	 		let y= parseInt(strs[1]);
		 	 		input.value=x+"-"+y;

		 	 		let event=new Event();
			    	 event.senderObj=that;
					 event.type=EventType.INTRACTIVE;

					 event.eventData={
						"action":"location", 							//动作，拖拽
						"coordPosition":[x,y] 	    				//定位点
					};
					EventCenter_EventCenter.postEvent(event);
					return;
		 	 	}
		 	 	
		 	 }

			alert("坐标不符合规范");

		 }
		

	}

	_initStyle(){
		let cssStr="  div.search{   height: 30px;   width: 130px;    }   div.search  input:nth-child(1){   width: 75px;   height: 100%;   font-size: 16px;   text-indent: 5px;   border: none;   float: left;  }  div.search  input:nth-child(2){   width: 50px;   height: 100%;   font-size: 16px;   letter-spacing: 5px;   border: none;  } ";
		loadStyle(cssStr);
	}
  
	

}


// CONCATENATED MODULE: ./src/js/intractive/MousePointer.js





class MousePointer_MousePointer{
	constructor(intractiveLayer){
		
		this.currentPosition;
		this.intractiveLayer=intractiveLayer;
		this.intractiveLayer._viewEle.style.cursor="default";
		//鼠标样式
		this.lastCursorStyle="default";
		this.cursorStyle="default";
		this.cursorStyleChange=false; //样式是否需要改变。true 需要改变 ，false-不需要改变/已经改变了
		//监听鼠标样式事件
		EventCenter_EventCenter.regist(this,[EventType.POINTCSS]);
	}


	listenerCall(event){

		if(event.type==EventType.POINTCSS){

			let data=event.eventData;
			let action=data.action;


			
			if(action=="cssChange"){
				//console.log("鼠标指针改变事件"+"  原："+this.cursorStyle+"新"+data.cursorStyle)
				this.changeCursorCss(data.cursorStyle)
			}
		}

	}
    
	changeCursorCss(cursorStyle){
		if(cursorStyle&&cursorStyle!=this.cursorStyle){
			this.lastCursorStyle=this.cursorStyle;
			this.cursorStyle=cursorStyle;
			this.cursorStyleChange=true;
			this.intractiveLayer._viewEle.style.cursor=this.cursorStyle;//改变鼠标样式
			
			
		}
	}
    


}


// CONCATENATED MODULE: ./src/js/layer/IntractiveLayer.js












class IntractiveLayer_IntractiveLayer{

	/*viewEle-用户交互操作的元素ele
	*/
	constructor(options) {

		let that=this;
		this._viewEle=options.viewEle;


		//operationMode-操作模式
		this.operationMode=OperationMode.MOVEMODE;

		//定义交互能力
		this.abilities=[]; //功能数组
		this.abilitiesMap={};//功能map { 操作模式：[功能对象1，功能对象2]}
		
		this.registeAbility(new Movable_Movable(that),OperationMode.MOVEMODE)  //拖拽移动功能
		this.registeAbility(new Selectable_Selectable(that),OperationMode.SELECTMODE) //选择功能
		this.registeAbility(new Scalable_Scalable(that),"ALL") //缩放工呢

		this.registeAbility(new ModeChangePan_ModeChangePan(that),"ALL")  	//模式控制面板
		this.registeAbility(new QuickLocationPan_QuickLocationPan(that),"ALL")  //快速定位面板
		this.registeAbility(new MousePointer_MousePointer(that),"ALL")  		//鼠标相关功能
		


		// this._viewEle.style.pointerEvents="none";


		this._viewEle.onmousedown=function(e){
			//console.log("IntractiveView onMouseDown")
			that.onmousedown=e;
			that.callAbility("onmousedown",e);

			//that.mouseDown(e);
		}

		this._viewEle.onmouseup=function(e){
			//console.log("IntractiveView onMouseUp")
			//that.mouseUp(e);
			//that.onmouseup=e;
			that.callAbility("onmouseup",e);
		}

		this._viewEle.onmousemove=function(e){
			that.onmousemove=e;
			//console.log("IntractiveView onMousemove")

			//that.mouseMove(e);
			//对单击操作中的mouseMove 过滤
			if(that.onmousedown){
				//console.log(that.onmousedown)
				//console.log(that.onmousemove)
				if(that.onmousedown.x==that.onmousemove.x&&that.onmousedown.y==that.onmousemove.y){
					return;
				}
				
			}
			
			that.callAbility("onmousemove",e);
		}

		this._viewEle.onmouseover=function(e){
			//console.log("IntractiveView onmouseover")
			//that.mouseOver(e);
			that.callAbility("onmouseover",e);
		}

		this._viewEle.onmouseout=function(e){
			//console.log("IntractiveView onmouseout")
			//that.mouseOut(e);
			that.callAbility("onmouseout",e);
		}

		this._viewEle.onclick=function(e){
			//console.log("IntractiveView onmouseout")
			//that.mouseOut(e);
			that.callAbility("onclick",e);
		}

		this._viewEle.onmousewheel=function(e){
			//console.log("IntractiveView onmouseout")
			//that.mouseOut(e);
			that.callAbility("onmousewheel",e);
		}


		EventCenter_EventCenter.regist(this,[EventType.NONE],this.eventCall);

    }


    /*注册能力对象，并将对象能力绑定到模式上
	abilitie-功能对象，仅限于intractive包下的对象
	operationMode-操作模式， 如果为“ALL” 表示所有模式都试用
    */
    registeAbility(abilitie,operationMode){

    	this.abilities.push(abilitie);
    	
    	if(operationMode in this.abilitiesMap){
    		this.abilitiesMap[operationMode].push(abilitie);
    	}else{
    		this.abilitiesMap[operationMode]=[abilitie];
    	}
    	
    }


    /*根据模式获取能力对象
	mode-OperationMode
	return-[abilities,abilities] 功能数组
    */
    getAbilitesByOperationMode(mode){
    	if(! (mode in OperationMode)){
    		throw "非法的操作模式"
    	}
    	
    	//获取指定模式下的功能
    	let abilities=[];
    	if(mode in this.abilitiesMap){
			abilities=abilities.concat(this.abilitiesMap[mode]);	
    	}

    	//获取all模式下的功能
    	abilities=abilities.concat(this.abilitiesMap["ALL"]);	

    	return abilities;
    }

    /*调用能力
	functionName-功能的方法名称
    */
    callAbility(functionName,event){
    	//获取当前操作模式下的功能列表
    	let abilities=this.getAbilitesByOperationMode(this.operationMode);
    	
    	for(var i=0;i<abilities.length;i++){

    		//执行方法
			let abilitie=abilities[i];
			let realfunction=abilitie[functionName];
			if(!realfunction){
				//方法不存在就不会调用
			}else{
				realfunction.call(abilitie,event)

			}

    	}
    }

    /*事件处理方法
	event-事件类
    */
    eventCall(event){

    	console.log("收到事件");
    	console.log(event);
    }

   	setOperationMode(mode){
   		if(! (mode in OperationMode)){
    		throw "非法的操作模式"
    	}
    	this.operationMode=mode;
   	}

   /*	获取视图在浏览器中的偏移量
	用于计算鼠标在layer中的位置
	return-[offsetX,offsetY] x y的偏移量
   */
   	getViewOffset(){
   		


   		return sumOffset(this._viewEle);
   	}

}

let OperationMode={
	"MOVEMODE":"MOVEMODE",  //移动模式
	"SELECTMODE":"SELECTMODE" //选择模式
}


// CONCATENATED MODULE: ./src/js/coordinateSystem/BaseSystem.js

class BaseSystem{


	constructor(IBase) {

		//定义单位距离的像素数量
		this.pxPerUnit=16;  
       
    }

    getExtenCoordinate(coo){



    }

    /*转换不同坐标系的坐标
	coord           -来源坐标系坐标
	width           -视图宽
	height          -视图高
	fromOriginMode  -来源坐标系模式-ORIGIN_MODE
	toOriginMode    -目标坐标系模式-ORIGIN_MODE
    */
    translateCoordToDifferentSystem(coord,width,height,fromOriginMode,toOriginMode){
    	let name=fromOriginMode+toOriginMode;
    	if(name=="LTLB"||name=="LBLT"){
    		//y-相反
    		coord.y=height-coord.y;
    	}else if(name=="LTRT"){
    		//x-相反
    		coord.x=width-coord.x;
    	}else if(name=="LTRB"){
    		//x-y 相反
    		coord.x=width-coord.x;
    		coord.y=height-coord.y;

    	}else if(name=="LBRT"){
    		//x-y 相反
    		coord.x=width-coord.x;
    		coord.y=height-coord.y;
    	}else if(name=="LBRB"){
    		//x-相反
    		coord.x=width-coord.x;
    	}else if(name=="RTRB"){
    		//y-相反
    		coord.y=height-coord.y;
    	}
    	return coord;
    	
    }

}

let ORIGIN_MODE={
	"LT":"LT",
	"LB":"LB",
	"RT":"RT",
	"RB":"RB",

}

// CONCATENATED MODULE: ./src/js/coordinateSystem/position.js


//像素位置，从坐标系原点计算
class PXPosition{

	constructor(x,y){
		this.x=x;
		this.y=y;
	}

}

//坐标位置,从坐标系原点计算
class CoordPosition{
	constructor(x,y){
		this.x=x;
		this.y=y;
	}
}


//视图区域的像素位置，从视图原点计算
class ViewPXPosition{

	constructor(x,y){
		this.x=x;
		this.y=y;
	}

}



// CONCATENATED MODULE: ./src/js/coordinateSystem/CoordinateSystem.js






class CoordinateSystem_CoordinateSystem{

	/*构造方法
	baseSystem-基础坐标系
	viewSize-[widthPx,heightPx]可视区像素范围大小
	*/
	constructor(baseSystem,viewSize) {

		this.coordinateSystem=baseSystem;
		
		//当前可视坐标系范围的中心点
		//this.originCenterPX=new PXPosition(-1,-1); // 初始视图坐标系位置
		this.centerPx=new PXPosition(0,0); //当前的视图中心坐标系像素位置

		//原始为2
		this.scaleSize=2;
		this.pxPerUnitWidth=SCALESIZE[this.scaleSize].pxPerUnitWidth; //单位长的像素距离
		this.pxPerUnitHeight=SCALESIZE[this.scaleSize].pxPerUnitHeight;//单位高的像素距离

		this.viewSize=viewSize;
		
		//初始化视图中心为，view的中心点
		this.setViewCenter(viewSize[0]/2,viewSize[1]/2);

	 }

	 /*对坐标系层级缩放（区别于scaleSystem，scaleSystem是对视图网格放大）
	   层级关系为2倍 缩小或者放大(该条件为必须)
	   缩放效果-锚点中心缩放
	 */
	 scaleLevel(startViewPXPosition,scaleDelta){

	 	/*20210108- 无解。。。当前缩放效果为视图中心缩放

	 	*/
	 	//计算锚点得到坐标
		let anchorPxPosition=this.getPxPositionFromViewPxPosition(startViewPXPosition);
		//重新计算中心坐标轴像素坐标
		//计算 centerPx 的点 到 startViewPXPosition的坐标偏移量
		let xOffsetPx=startViewPXPosition.x -this.viewSize[0]/2;
		let yOffsetPx=startViewPXPosition.y - this.viewSize[1]/2;
		if(scaleDelta>0){//放大
			anchorPxPosition.x= anchorPxPosition.x*2
			anchorPxPosition.y= anchorPxPosition.y*2
		}else if(scaleDelta<0){  //缩小
			anchorPxPosition.x= anchorPxPosition.x/2
			anchorPxPosition.y= anchorPxPosition.y/2
		}

		//计算 centerPx 的像素坐标
		this.centerPx.x=Math.floor(anchorPxPosition.x-xOffsetPx);
		this.centerPx.y=Math.floor(anchorPxPosition.y-yOffsetPx);
	 }


	 /*缩放坐标系
	 startViewPXPosition-锚点viewPXPosition
	 scaleDelta-缩放变化量
	 return 超出最大范围 1，超出最小范围-1 ，有效值0
	 */
	scaleSystem(startViewPXPosition,scaleDelta){

		//计算锚点得到坐标
		let anchorCoordPosition=this.getCoordinateFromViewPxPositionExact(startViewPXPosition);



		//设置缩放级别及相关参数
		let scaleTemp=this.scaleSize+scaleDelta;
		let flag=this.setScaleSize(scaleTemp);

		if(0==flag){//缩放数据有效
			
			//设置缩放级别后，原originCenterPX不会变化，不用计算，centerPx 会产生变化
			//缩放后锚点的坐标及viewPXPosition都不会变化

			//计算 centerPx 的点 到 startViewPXPosition的坐标偏移量
			let xOffsetCoord=(this.viewSize[0]/2-startViewPXPosition.x)/this.pxPerUnitWidth;
			let yOffsetCoord=(this.viewSize[1]/2-startViewPXPosition.y)/this.pxPerUnitHeight;

			let viewCenterCoord=new CoordPosition(anchorCoordPosition.x+xOffsetCoord,anchorCoordPosition.y+yOffsetCoord);
			//计算 centerPx 的像素坐标
			this.centerPx.x=Math.floor(viewCenterCoord.x*this.pxPerUnitWidth) ;
			this.centerPx.y=Math.floor(viewCenterCoord.y*this.pxPerUnitHeight);
		}

		
		return flag;
	}


	/*设置坐标系的缩放级别  SCALSIZE_LIMIT.max SCALSIZE_LIMIT.min 缩放级别最大和最小值范围
	scaleSize-目标缩放大小
	return- 在正常范围 0 ，超出最大范围 1，超出最小范围 -1
	
	*/
	setScaleSize(scaleSize){
		

		if(scaleSize>SCALSIZE_LIMIT.max){
			//超出范围不做响应
			return 1;
		}else if(scaleSize<SCALSIZE_LIMIT.min){
			//超出范围不做响应
			return -1;
		}

		this.scaleSize=scaleSize;
		this.pxPerUnitWidth=SCALESIZE[scaleSize].pxPerUnitWidth; //单位长的像素距离
		this.pxPerUnitHeight=SCALESIZE[scaleSize].pxPerUnitHeight;//单位高的像素距离

		return 0;
	}
   
    /*设置坐标系视图中心的像素坐标点
	 x-x坐标
	 y-y坐标
    */
    setViewCenter(x,y){
    	//如果原始原点不存在则设置
    	if(!this.originCenterPX){
    		this.originCenterPX=new PXPosition(x,y);
    	}

    	this.centerPx.x=x;
    	this.centerPx.y=y;
    }

    /*设置坐标系视图中心的坐标位置
	x-x坐标
	y-y坐标
    */
    setCoordCenter(x,y){
    	//将坐标转换为像素坐标
    	let pxPosition=new PXPosition(
    		x*this.pxPerUnitWidth+this.pxPerUnitWidth/2,y*this.pxPerUnitHeight+this.pxPerUnitHeight/2
    		);
    	this.setViewCenter(pxPosition.x,pxPosition.y)

    }

   /* 
   	获取坐标系的中心坐标（非像素坐标）
   */
    getCoordCenter(){
    	let coordX=(this.centerPx.x-this.pxPerUnitWidth/2)/this.pxPerUnitWidth;
    	let coordY=(this.centerPx.y-this.pxPerUnitHeight/2)/this.pxPerUnitHeight;
    	return [coordX,coordY]
    }

   /* 移动坐标系视图中心像素坐标点
	xdistance-x 轴移动距离
	ydistance-y轴移动距离
   */
    moveViewCenter(xdistance,ydistance){
    	this.centerPx.x+=xdistance;
    	this.centerPx.y+=ydistance;
    }



    /*设置锚点
	xCoordPx-x 在当前坐标系内的像素横坐标
	yCoordPx-y 在当前坐标系内的像素横坐标
    */
    setAnchor(xCoordPx,yCoordPx){

	}


	

	translatePxCoordinate(position){
		this.coordinateSystem.translatePxPosition();
	}


	/*对canvas视图坐标，转换为坐标系坐标。不计算便宜量，用于绘图逻辑视图坐标
	viewPxPosition-像素坐标
	return-viewPxPosition像素坐标
	*/
	translateFromCanvasXY(viewPxPosition,canvas){
		return this.coordinateSystem.translateFromCanvasXY(viewPxPosition,canvas);
	}

	/*对坐标系视图坐标，转化为canvas坐标。不计算偏移量，用于在canva绘图
	viewPxPosition-像素坐标
	return-viewPxPosition像素坐标
	*/
	translateToCanvasXY(viewPxPosition,canvas){
		return this.coordinateSystem.translateToCanvasXY(viewPxPosition,canvas);
	}


	/*对canvas移动距离转换为坐标系内的移动距离（主要转换方向）
	xyCanvasDistance-[xdistance,ydistance]  canvas的像素移动距离
	return-[xdistance,ydistance] 坐标系的移动距离
	*/
	translateDistanceFromCanvasXY(xyCanvasDistance){
		return this.coordinateSystem.translateDistanceFromCanvasXY(xyCanvasDistance);
	}


	/*获取像素区域所显示坐标轴范围
	startViewPxPosition- 起始视图区的像素位置（不含坐标系偏移）
	endViewPxPosition-结束视图区的像素位置（不含坐标系偏移） 
	return-坐标范围[startX,startY,endX,endY]
	*/
	getCoordinateExtendByViewExtend(startViewPxPosition,endViewPxPosition){
		let offsetXY=this.getSystemPxOffset();
		//将像素位置转换为，坐标系像素位置（含偏移量）
		let startPxPosition=new PXPosition(startViewPxPosition.x+offsetXY[0],startViewPxPosition.y+offsetXY[1]);
		let endPxPosition=new PXPosition(endViewPxPosition.x+offsetXY[0],endViewPxPosition.y+offsetXY[1])

		//计算像素点所在的坐标区域
		let minX=startPxPosition.x;
		let minY=startPxPosition.y;
		let maxX=endPxPosition.x;
		let maxY=endPxPosition.y

		if(minX>maxX){
			minX=endPxPosition.x;
			maxX=startPxPosition.x;
		}

		if(minY>maxY){
			minY=endPxPosition.y;
			maxY=startPxPosition.y
		}



		let pxPositionExtend=[]
		

		let x1=Math.floor( minX / this.pxPerUnitWidth);
		let y1=Math.floor( minY / this.pxPerUnitHeight);

		let x2=Math.floor(maxX / this.pxPerUnitWidth);
		let y2=Math.floor(maxY / this.pxPerUnitHeight);

		

		return [x1,y1,x2,y2]
	}


	/*根据坐标获取像素坐标（含偏移量）

	*/
	getPxPositionFromCoordinate(coordX,coordY){
		return new PXPosition(coordX*this.pxPerUnitWidth,coordY*this.pxPerUnitHeight)
	}

	/*获取视图像素位置的坐标-不带小数

	*/
	getCoordinateFromViewPxPosition(viewPxPosition){
		//计算pxPosition
		let systemOffset=  this.getSystemPxOffset();
		let pxPosition=new PXPosition(viewPxPosition.x+systemOffset[0],viewPxPosition.y+systemOffset[1])
		//计算坐标

		let coordX=Math.floor(pxPosition.x/this.pxPerUnitWidth) ;
		let coordY=Math.floor(pxPosition.y/this.pxPerUnitHeight) ;
		return new CoordPosition(coordX,coordY);
	}

	/*20210109
	获取坐标抽像素坐标-带小数
	viewPxPosition-视图像素位置（canvas）
	其中混杂了对坐标系的转换--不明了。应该通过统一转换
	*/
	getPxPositionFromViewPxPosition(viewPxPosition){
		//获取视图中心点的像素坐标
		let viewCenter=this.getViewCenter();
		//获取指定视图点到视图中心点的像素偏移量
		let coord=viewPxPosition;
		let width=this.viewSize[0];
		let height=this.viewSize[1];
		let systemViewPosition=this.coordinateSystem.translateCoordToDifferentSystem(coord,width,height,ORIGIN_MODE.LT,ORIGIN_MODE.LB);
		
		let xPxOffset= systemViewPosition.x-this.viewSize[0]/2;
		let yPxOffset= systemViewPosition.y-this.viewSize[1]/2;

		//计算指定点的像素坐标
		let pxPosition=new PXPosition(viewCenter.x+xPxOffset,viewCenter.y+yPxOffset)
		return pxPosition;
	}
	/*20210109
	获取坐标抽 坐标-不带小数
	viewPxPosition-视图像素位置（canvas）
	其中混杂了对坐标系的转换--不明了。应该通过统一转换
	*/
	getCoordFromViewPxPosition(viewPxPosition){
		let that=this;
		let pxPosition=this.getPxPositionFromViewPxPosition(viewPxPosition);
		return new CoordPosition(Math.floor(pxPosition.x/that.pxPerUnitWidth),Math.floor(pxPosition.y/that.pxPerUnitHeight));
	}

	/*获取视图像素位置的坐标-带小数
	*/
	getCoordinateFromViewPxPositionExact(viewPxPosition){
		//计算pxPosition
		let systemOffset=  this.getSystemPxOffset();
		let pxPosition=new PXPosition(viewPxPosition.x+systemOffset[0],viewPxPosition.y+systemOffset[1])
		//计算坐标

		let coordX=pxPosition.x/this.pxPerUnitWidth ;
		let coordY=pxPosition.y/this.pxPerUnitHeight ;
		return new CoordPosition(coordX,coordY);
	}


	/*获取坐标在当前视图中的像素坐标区域，
	如果当前坐标已超出可视范围，则返回 [ ViewPXPosition(-1,-1),ViewPXPosition(-1,-1)]
	coordX-x坐标
	coordY-y坐标
	return-
	*/
	getViewPxPositionExtendFromCoordinate(coordX,coordY,canvas){


		let pxPosition=this.getPxPositionFromCoordinate(coordX,coordY);
		let systemOffset=  this.getSystemPxOffset();
		let viewPxPosition=new ViewPXPosition(pxPosition.x-systemOffset[0],pxPosition.y-systemOffset[1])

		//判断该点是否超出可视区(可视区，因为是以右下角为起点，所以最小值因>-pxPerUnitWidth,高也是如此)
		let outOfView=0;
		if(viewPxPosition.x+this.pxPerUnitWidth<0 ||viewPxPosition.x>canvas.width){
			outOfView=1 ;

		}

		if(viewPxPosition.y+this.pxPerUnitHeight<0||viewPxPosition.y>canvas.height){
			outOfView=1 ;
		}

		if(outOfView==1){
			return [ new ViewPXPosition(-1,-1),new ViewPXPosition(-1,-1)]
		}else{
			/*this.pxPerUnitWidth=16; //单位长的像素距离
			this.pxPerUnitHeight=16;//单位高的像素距离*/
			let startViewPxPosition=viewPxPosition;
			let endViewPxPosition=new ViewPXPosition(startViewPxPosition.x+this.pxPerUnitWidth,startViewPxPosition.y+this.pxPerUnitHeight);
			
			return [startViewPxPosition,endViewPxPosition]
		}
		

		
	}


	/*获取坐标系像素偏移量
	return-[xPxOffset,yPxOffset]
	*/
	getSystemPxOffset(){
		let origin=this.originCenterPX; 
		let now=this.centerPx;
		return [now.x-origin.x,now.y-origin.y];
	}

	/*获取当前视图中心的坐标系像素坐标-含偏移量

	*/
	getViewCenter(){
		return this.centerPx;
	}


	/*获取当前canvas视图中包含的坐标范围
	*/
	getViewCoordExtend(canvas){
		
		let startViewPxPosition=new ViewPXPosition(0,0);
		let endViewPxPosition=new ViewPXPosition(canvas.width,canvas.height);
		return this.getCoordinateExtendByViewExtend(startViewPxPosition,endViewPxPosition)
	}

	/*获取 区域 相对于坐标系 的起点，即由相对坐标系 minX minY组成的点
	*/
	getStartPointFromCanvasViewPXPositionExtend(viewPxPosition1,viewPxPosition2){
		return this.coordinateSystem.getStartPointFromCanvasViewPXPositionExtend(viewPxPosition1,viewPxPosition2);
	}
}

/*缩放级别对应的尺寸*/
let SCALESIZE={
	1:{"pxPerUnitWidth":8,"pxPerUnitHeight":8},
	2:{"pxPerUnitWidth":16,"pxPerUnitHeight":16},
	3:{"pxPerUnitWidth":24,"pxPerUnitHeight":24},
	4:{"pxPerUnitWidth":32,"pxPerUnitHeight":32},
	5:{"pxPerUnitWidth":40,"pxPerUnitHeight":40},
	6:{"pxPerUnitWidth":48,"pxPerUnitHeight":48},
	7:{"pxPerUnitWidth":56,"pxPerUnitHeight":56},
	8:{"pxPerUnitWidth":64,"pxPerUnitHeight":64},
	9:{"pxPerUnitWidth":72,"pxPerUnitHeight":72},
	10:{"pxPerUnitWidth":80,"pxPerUnitHeight":80}
	
}

let SCALSIZE_LIMIT={
	min: 1,
	max: 10
}




// CONCATENATED MODULE: ./src/js/coordinateSystem/CartesianSystem.js




class CartesianSystem_CartesianSystem extends BaseSystem{

	constructor() {
		super();
       
    }

    /*对canvas视图坐标，转换为坐标系坐标。不计算偏移量，用于绘图逻辑视图坐标
	pxPosition-像素坐标
	*/
	translateFromCanvasXY(pxPosition,canvas){
		return new ViewPXPosition(pxPosition.x,canvas.clientHeight-pxPosition.y);
	}

	/*对坐标系视图坐标，转化为canvas坐标。不计算偏移量，用于在canva绘图
	pxPosition-像素坐标
	*/
	translateToCanvasXY(pxPosition,canvas){
		return new ViewPXPosition(pxPosition.x,canvas.clientHeight-pxPosition.y);
	}

	/*对canvas移动距离转换为坐标系内的移动距离（主要转换方向）
	xyCanvasDistance-[xdistance,ydistance]  canvas的像素移动距离
	return-[xdistance,ydistance] 坐标系的移动距离
	*/
	translateDistanceFromCanvasXY(xyCanvasDistance){
		//将x轴距离变换方向即可
		return [0-xyCanvasDistance[0],xyCanvasDistance[1]];
	}


	getStartPointFromCanvasViewPXPositionExtend(viewPxPosition1,viewPxPosition2){
		//获取4角顶点
		let x1=viewPxPosition1.x<=viewPxPosition2.x?viewPxPosition1.x:viewPxPosition2.x;
		let y1=viewPxPosition1.y<=viewPxPosition2.y?viewPxPosition1.y:viewPxPosition2.y;
		
		let x2=viewPxPosition1.x>=viewPxPosition2.x?viewPxPosition1.x:viewPxPosition2.x;
		let y2=viewPxPosition1.y>=viewPxPosition2.y?viewPxPosition1.y:viewPxPosition2.y;

		/*在笛卡尔坐标系中因选取左下角的点作为起点*/

		return new ViewPXPosition(x1,y1);
	}
}


// CONCATENATED MODULE: ./src/js/draw/Draw.js


class Draw{

	constructor(canvasDraw){
		this.canvasDraw=canvasDraw;
		this.gridLineColor="#B6C29A"
	}

    /*画网格
    offsetX-第一条纵线偏移量
    offsetY-第一条横线偏移量
    pxPerUnitWidth-单位宽像素距离
    pxPerUnitHeight-单位高像素距离
    */
	drawGrid(offsetX,offsetY,pxPerUnitWidth,pxPerUnitHeight){
        //去掉小数点-用于控制线条模糊度问题
          offsetX=Math.floor(offsetX);
          offsetY=Math.floor(offsetY);
		if(offsetX>=pxPerUnitWidth||offsetY>=pxPerUnitHeight){
			console.log(offsetX,offsetY,pxPerUnitWidth,pxPerUnitHeight);
			throw "偏移量应小于单位距离"
		}
			var ctx = this.canvasDraw;
			ctx.strokeStyle = this.gridLineColor;
            ctx.lineWidth = 1;

            //偏移0.5的量是因为canvas中渲染线条更清晰
            //画横线
            let sysOffsetX=0.5;
            let sysOffsetY=0.5;
            offsetY=offsetY+sysOffsetY;
            offsetX=offsetX+sysOffsetX;
            for(var i =offsetY;i<ctx.canvas.clientHeight;i+=pxPerUnitHeight){
                ctx.beginPath();
                ctx.moveTo(0,i);
                ctx.lineTo(ctx.canvas.clientWidth,i);
                ctx.stroke();
            }

            //画竖线
            for(var i = offsetX;i<ctx.canvas.clientWidth;i+=pxPerUnitWidth){
                ctx.beginPath();
                ctx.moveTo(i,0);
                ctx.lineTo(i,ctx.canvas.clientHeight);
                ctx.stroke();
            }
          	

             ctx.closePath();
	}


	cleanGrid(){
         var ctx = this.canvasDraw;
         ctx.clearRect(0,0,ctx.canvas.width,ctx.canvas.height);
    }

    /*画出选择区域(对外应提供 按坐标系的逻辑规则参数，而不是canva的逻辑规则参数)
    */
    drawSelected(leftTopViewPxPosition,width,height){
         var ctx = this.canvasDraw;
         //画出矩形  
         let color='rgba(0, 255, 127, 0.5)';
         ctx.fillStyle = color;
         ctx.fillRect(leftTopViewPxPosition.x,leftTopViewPxPosition.y,width,height);
         ctx.closePath();

    }

    /*画出选择区域的文本(对外应提供 按坐标系的逻辑规则参数，而不是canva的逻辑规则参数，待修改)
    leftBottomViewPxPosition-左下角的点
    rightTopViewPxPosition-右上角的点
    leftBottomText-左下角文字
    rightTopText-右上角文字
    canvasWidth-画布宽度
    canvasHeight-画布高度
    */
    drawSelectText(leftBottomViewPxPosition,rightTopViewPxPosition,leftBottomText,rightTopText,canvasWidth,canvasHeight){
        let fillStyle = "red";               //设置填充颜色为紫色
        let font = '20px "微软雅黑"';           //设置字体
        //画出范围坐标右上角
        let txt1=rightTopText;

        let textAlign1="left";
        let textBaseline1="bottom";
        if((canvasWidth-rightTopViewPxPosition.x)<45)
            textAlign1="right";

        if((canvasHeight-rightTopViewPxPosition.y)<20)
            textBaseline1="top";
        
        this.drawText(rightTopViewPxPosition.x,rightTopViewPxPosition.y,txt1,textAlign1,textBaseline1,fillStyle,font);
        
         //画出范围坐标左下角 

         if(leftBottomText==rightTopText){
            //如果两个点的坐标一致，则只画一个坐标点文字就可以了
            return;
         }

        let txt2=leftBottomText;

        let textAlign2="right";
        let textBaseline2="top";;
        if(leftBottomViewPxPosition.x<45)
            textAlign2="left";

        if(leftBottomViewPxPosition.y<20)
            textBaseline2="bottom";
        this.drawText(leftBottomViewPxPosition.x,leftBottomViewPxPosition.y,txt2,textAlign2,textBaseline2,fillStyle,font); 


    }


    drawGridText(viewPxPosition,text,canvasWidth,canvasHeight){
        let fillStyle = "red";               //设置填充颜色为紫色
        let font = '20px "微软雅黑"';           //设置字体
        //画出范围坐标右上角
        let txt1=text;

        let textAlign1="left";
        let textBaseline1="bottom";;
        if((canvasWidth-viewPxPosition.x)<45)
            textAlign1="right";

        if((canvasHeight-viewPxPosition.y)<20)
            textBaseline1="top";
        
        this.drawText(viewPxPosition.x,viewPxPosition.y,txt1,textAlign1,textBaseline1,fillStyle,font);
        
    }


    drawText(viewPxX,viewPxY,text,textAlign,textBaseline,fillStyle,font){
        var ctx = this.canvasDraw;
        ctx.fillStyle = fillStyle;               //设置填充颜色为紫色
        ctx.font =font;           //设置字体
        ctx.textAlign=textAlign;
        ctx.textBaseline=textBaseline;
        //ctx.strokeText( "left", 450, 400 );
        ctx.fillText( text, viewPxX, viewPxY );        //填充文字
        ctx.closePath();
    }



    /*填充单个网格 (对外应提供 按坐标系的逻辑规则参数，而不是canva的逻辑规则参数，待修改)
    unitDataArray-填充数据数组
    */
    fillGrid(unitDataArray){
        var ctx = this.canvasDraw;
        //循环填充区域
        for(let i=0;i<unitDataArray.length;i++){
             

             let item=unitDataArray[i];
             
             ctx.fillStyle = item.color;
             //+1  -1的原因，因为因为保证不遮盖掉网格线。
             //为什呢y是0.5.中间可能有计算偏移了 部分像素点-以后可能需要改
             ctx.fillRect(item.pxX+1,item.pxY+0,item.pxWidth-1,item.pxHeight-1 );
        }
        ctx.closePath();
    }

   /* 对视图进行文字填充。标注

   */
    fillText(unitDataArray){
        var ctx = this.canvasDraw;
        //循环填充区域
        for(let i=0;i<unitDataArray.length;i++){
             
            let item=unitDataArray[i];
           
             if(!item.data||!item.data.text){
                continue;
             }
             let text=item.data.text;
             let fillStyle = text.color;
             let font=text.size+"pt Calibri";
             this.drawText(item.pxX,item.pxY,text.content,"center","middle",fillStyle,font)

        }
    }
}


class FillData{
    
    constructor(viewPxX,viewPxY,width,height,color){
        this.viewPxX=viewPxX;
        this.viewPxY=viewPxY;
        this.width=width;
        this.height=height;
        this.color=color;
    }
}

// CONCATENATED MODULE: ./src/js/draw/canvas/CanvasDraw.js



/*主要对canvas 画图进行封装
所有坐标参数均使用笛卡尔坐标系 dcs

*/
class CanvasDraw_CanvasDraw{

   /*构造方法
    canvas-canvas对象
    coorinateSystem-坐标系对象
   */
   constructor(canvas,coorinateSystem) {
		  this.canvas=canvas;
      this.coorinateSystem=coorinateSystem;
      this.ctx = canvas.getContext("2d");
    }


   /* 画填充矩形
    */
   fillRect(x,y,width,height){
    let  canvasXY= this.translateToCanvasXY([x,y] )
     if(this.fillStyle){
       this.ctx.fillStyle=this.fillStyle;
     }
      this.ctx.fillRect(canvasXY[0],canvasXY[1],width,height);
   }

  

   beginPath(){
      this.ctx.beginPath();
   }

   closePath(){
   	 this.ctx.closePath();
   }

   

   stroke(){
    if(this.strokeStyle)
    this.ctx.strokeStyle=this.strokeStyle
   	 this.ctx.stroke();
   }

   lineTo(x,y){
     let canvasXY= this.translateToCanvasXY([x,y] )
   	 this.ctx.lineTo(canvasXY[0],canvasXY[1]);
    // console.log("lineTo-["+canvasXY[0]+","+canvasXY[1]+"]"  )
   }

   moveTo(x,y){
    let canvasXY= this.translateToCanvasXY([x,y] );
   	 this.ctx.moveTo(canvasXY[0],canvasXY[1]);
    // console.log("moveTo-["+canvasXY[0]+","+canvasXY[1]+"]" )
   }

   clearRect(x,y,width,height){
     let canvasXY= this.translateToCanvasXY([x,y] );
     this.ctx.clearRect(canvasXY[0],canvasXY[1]-height,width,height);
   }
  

  /* 对相应的坐标系（原点不同方向）像素坐标进行转换成canva像素坐标
  xy-[x,y] 在坐标系中的视图像素坐标
  return-[x,y] 在canva中的视图像素坐标
  */
   translateToCanvasXY(xy){
     let originPosition=new ViewPXPosition(xy[0],xy[1]);
     let viewPxPosition= this.coorinateSystem.translateToCanvasXY(originPosition,this.canvas);
     return [viewPxPosition.x,viewPxPosition.y]

   }

   
   fillText( text, x, y){
        let canvasXY= this.translateToCanvasXY([x,y] );

        var ctx = this.ctx;
        if(this.fillStyle)
          ctx.fillStyle =this.fillStyle;  //设置填充颜色为紫色
       
        if(this.fillStyle)             
          ctx.font =this.font;           //设置字体\
       
        if(this.fillStyle)
          ctx.textBaseline =this.textBaseline;            //设置字体底线对齐绘制基线
       
        if(this.fillStyle)
          ctx.textAlign =this.textAlign;                 //设置字体对齐的方式
       
       ctx.fillText( text, canvasXY[0], canvasXY[1] );        //填充文字

       
    }

    strokeText( text, x, y){
        let canvasXY= this.translateToCanvasXY([x,y] );

        var ctx = this.ctx;
        if(this.fillStyle)
          ctx.fillStyle =this.fillStyle;  //设置填充颜色为紫色
       
        if(this.fillStyle)             
          ctx.font =this.font;           //设置字体\
       
        if(this.fillStyle)
          ctx.textBaseline =this.textBaseline;            //设置字体底线对齐绘制基线
       
        if(this.fillStyle)
          ctx.textAlign =this.textAlign;                 //设置字体对齐的方式
       
       ctx.strokeText( text, canvasXY[0], canvasXY[1] );        //填充文字

       
    }

     /*设置字体
     font-字体，包含大小及格式，如 40pt Calibri
     */
    setFont(font){
        ctx.font(font);
    }

}


// CONCATENATED MODULE: ./src/js/data/RenderData.js





class RenderData_GridDataCenter{

	constructor(dataSource){
		EventCenter_EventCenter.regist(this,[EventType.NONE]);

		this.blockDatas={}; //{ "xScale_yScale":blockData }
		
		//数据源因为通用 所以绑定到类上
		RenderData_GridDataCenter.dataSource=dataSource;

		//设置Z层级
		this.zIndex=-1; //-1表示初始化值，并且不启用
		
	}

	/*设置数据源的层级*/
	setZIndex(zIndex){
		if(!zIndex){
			console.log("z轴开启无效")
			//设置Z层级
			return;
		}
		if(this.zIndex!=zIndex){
			//因为不会缓存层级数据，所以每次切换都需要清空原来数据
			this.blockDatas={};
			this.zIndex=zIndex;
		}
	}



	//重新设置dataSource
	setDataSource(dataSource){
		RenderData_GridDataCenter.dataSource=dataSource;
	}

	setUnitData(unitData){
		//计算偏移倍数
		let scaleNum=BlockData.calculateScale(unitData.x,unitData.y);
		let key=scaleNum[0]+"_"+scaleNum[1];
		

		//将数据添加到分片中
		let targetBlockData=this.blockDatas[key];
		
		if(!targetBlockData)
		{
			targetBlockData=new BlockData(this.zIndex,scaleNum[0],scaleNum[1]);
			this.blockDatas[key]=targetBlockData;
		}
		targetBlockData.set(unitData);
		this.onDataChange();
	}

	/*获取区域范围内的渲染数据
	startCoordinatePosition-指定区域的起始坐标
	endCoordinatePosition-指定区域的结束坐标
	return-[unitData1,unitData2] 数据数组
	*/
	getExtendData(startCoordinatePosition,endCoordinatePosition){
		
		let unitDataArray=[];
		//计算区域
		let scales=BlockData.calculateScaleExtend(startCoordinatePosition,endCoordinatePosition);
		for(let i=0;i<scales.length;i++){
			let scaleNum=scales[i];
			
			//获取区块数据
			let targetBlockData=this.getAndsetInNotExistBlockDataByScale(scaleNum[0],scaleNum[1]);
			
			let blockAllData=targetBlockData.getAll();
			//过滤掉不在指定范围内的数据
			for(let ii=0;ii<blockAllData.length;ii++){
				let unitData=blockAllData[ii];
				
				//判断数据是否在指定坐标区域内
				if(unitData.x>=startCoordinatePosition.x&&unitData.x<=endCoordinatePosition.x){
					if(unitData.x>=startCoordinatePosition.x&&unitData.x<=endCoordinatePosition.x){
						//符合要求
						unitDataArray.push(unitData);
					}
				}
			}

		}

		//返回区域的所有数据
		return unitDataArray;
	}

	/*获取单个数据
	x-x坐标
	y-y坐标
	return-unitData
	*/
	getUnitData(x,y){
		//计算偏移倍数
		let scaleNum=BlockData.calculateScale(x,y);

		let targetBlockData=this.getBlockDataByScale(scaleNum[0],scaleNum[1]);
		
		if(!targetBlockData){
			this.blockDatas[key]=new BlockData(this.zIndex,scaleNum[0],scaleNum[1]);
			targetBlockData=this.blockDatas[key];
		}
		//交由底层的分片块 去获取数据，从缓存获取，或者让从数据源获取
		return targetBlockData.get(x,y);
		
	}

	/*获取指定倍率的blockData
	xScale-x倍率
	yScale-y倍率
	*/
	getBlockDataByScale(xScale,yScale){
		let key=xScale+"_"+yScale;
		//将数据添加到分片中
		let targetBlockData=this.blockDatas[key];
		return targetBlockData;
	}

	/*获取blockdata，如果不存在则初始化
	xScale-x倍率
	yScale-y倍率
	return-blockData
	*/
	getAndsetInNotExistBlockDataByScale(xScale,yScale){
		let key=xScale+"_"+yScale;

		//如果不存在则，将数据设置进去
		if(!this.blockDatas[key]){
			this.blockDatas[key]=new BlockData(this.zIndex,xScale,yScale,this.onDataChange());
		}
		return  this.blockDatas[key];
	}

	onDataChange(){
			let that=this;
			return function(){
				console.log("onDataChange 数据源有变化");
				let event=new Event();
		    	event.senderObj=that;
				event.type=EventType.DATAREFRESH;

				event.eventData={
					"action":"refresh", //动作，刷新数据
				};

		    	EventCenter_EventCenter.postEvent(event);
			}
			

	}

}



class UnitData{

	constructor(x,y,data){

		this.x=x;
		this.y=y;
		this.data=data;
	}
}

//将100*100的网格划分一个块
class BlockData{
	/*构造函数
	xScale-区块x轴偏移量
	yScale-区块y轴偏移量
	notifyFunction-区块数据变动通知方法*/

	//20210107 添加对层级数据的支持
	constructor(zIndex,xScale,yScale,notifyFunction){
		//区块的偏移倍数，如坐标在[0,0]-[99,99] 则偏移倍数为 （0，0）
		//如坐标在[100,100]-[199,199] 则偏移倍数为 （1，1）
		this.xScale=xScale;
		this.yScale=yScale;
		this.zIndex=zIndex; //zIndex=-1 ,默认使用getBlock方法
		this.arr = new Array(100);

		//初始化数据块
		this.refreshData(); 
		this.notifyFunction=notifyFunction;
		
	}

	

	/*刷新数据块内的数据

	*/
	refreshData(){
		let that=this;
		//从数据源获取
		//20210107 支持层级数据源获取
		
		RenderData_GridDataCenter.dataSource.getBlock(that.zIndex,this.xScale,this.yScale,function(datas){
			for(let i=0;i<datas.length;i++){
				let unitData=datas[i];
				that.set(unitData);
			}
			//通知数据有变动
			if(that.notifyFunction){
				that.notifyFunction();
			}
		
		});
		
	}

	set(unitData){
		//计算数组中x y坐标
		let arrayXY=this.calculateArrayPositionByCoord(unitData.x,unitData.y);
		let x=arrayXY[0];
		let y=arrayXY[1];

		if(this.arr[x]){
			this.arr[x][y]=unitData;
		}else{
			this.arr[x]=new Array(100);
			this.arr[x][y]=unitData;
		}
	}

	get(coordx,coordy){
		let arrayXY=this.calculateArrayPositionByCoord(coordx,coordy);
		let x=arrayXY[0];
		let y=arrayXY[1];
		if(!this.arr[x]){
			return;
		}
		let unitData=this.arr[x][y];
		//如果未获取到数据，则从数据源获取(通过刷新动作重新获取数据)
		if(!unitData){
			/*let z=17
			unitData=GridDataCenter.dataSource.get(x,y,z);
			//保存数据
			this.setUnitData(unitData);*/
		}
		return unitData;
	}

	//获取区块当前所有数据
	getAll(){
		//flat 不能处理负坐标问题
		//let newArray = this.arr.flat(1);
		let newArray=new Array();
		for(let i in this.arr){
			if(this.arr[i]){
				for(let ii in this.arr[i]){
					if(this.arr[i][ii]){
						newArray.push(this.arr[i][ii])
					}
				}
			}
		}
		return newArray;
	}

	/*根据坐标计算在blockdata 中的二维数组位置
	x-x坐标
	y-y坐标
	return-[x,y]在blockdata中的二维数组位置
	*/
	calculateArrayPositionByCoord(x,y){
		//因为 100*100的区域
		return [x % 100,y % 100 ];
	}

}

/*对坐标计算偏移倍数
*/
 BlockData.calculateScale=function(xPosition,yPosition){
 	return [Math.floor(xPosition/100),Math.floor(yPosition/100)];
}

BlockData.calculateScaleExtend=function(startCoordinatePosition,endCoordinatePosition){
	let startScale=BlockData.calculateScale(startCoordinatePosition.x,startCoordinatePosition.y);
	let endScale=BlockData.calculateScale(endCoordinatePosition.x,endCoordinatePosition.y);

	let scales=[];
	for(let i=startScale[0];i<=endScale[0];i++){
		for(let ii=startScale[1];ii<=endScale[1];ii++){

			scales.push([i,ii]);
		}
	}
	return scales;
}



// CONCATENATED MODULE: ./src/js/data/PagemapDataSource.js


class PagemapDataSource_PagemapDataSource{

	constructor(){

	}

	get(x,y,z,callback){
		if(x%2==0){
			data.color="#f0f";
		}else{
			data.color="#000";
		}
		let unitData=new UnitData(x,y,data);
		
		callback(data);
	}

	/*获取块数据
	scaleX-偏移倍率
	scaleY-偏移倍率
	callback-获取成功后回调
	return-unitData的数组形式
	*/
	getBlock(scaleX,scaleY,callback){

		let unitDatas=[];
		if(scaleX==1&&scaleY==1){

			unitDatas.push(new UnitData(1,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(2,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(3,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(4,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(5,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(6,1,{"color":"#87CEFA"}))



		}else{

		}
		callback(unitDatas);
	}


	/*获取块数据(支持Z)
	scaleX-偏移倍率
	scaleY-偏移倍率
	callback-获取成功后回调
	return-unitData的数组形式
	*/
	getBlockZ(scaleX,scaleY,z,callback){

		let unitDatas=[];
		if(scaleX==1&&scaleY==1){

			unitDatas.push(new UnitData(1,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(2,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(3,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(4,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(5,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(6,1,{"color":"#87CEFA"}))



		}else{

		}
		callback(unitDatas);
	}

}




// CONCATENATED MODULE: ./src/js/layer/GridLayer.js












/*底图层
主要功能-绘制相关网格 及 底图的文字


*/
class GridLayer_GridLayer{

	constructor(options) {
		let that=this;
		this.canvas=options.canvas;

		let viewSize=[this.canvas.width,this.canvas.height];
		this.coordinateSystem=new CoordinateSystem_CoordinateSystem(new CartesianSystem_CartesianSystem(),viewSize);
		//设置坐标系的中心点为canva画布的中心坐标。以为在中心所以不用考虑 坐标转换问题

		this.canvasDraw=new CanvasDraw_CanvasDraw(this.canvas,this.coordinateSystem);
		this.draw=new Draw(this.canvasDraw);

		
		//网格数据中心
		if(options.dataSource){
			this.gridDataCenter=new RenderData_GridDataCenter(options.dataSource);
		}else{
			this.gridDataCenter=new RenderData_GridDataCenter(new PagemapDataSource_PagemapDataSource());
		}

		 //初始化z的层级开关
		 this.maxZ=options.maxZ;
		 this.minZ=options.minZ;
		 //this.zIndex=options.zIndex;
		 //this.gridDataCenter.setZIndex(this.zIndex)
       	 this.setZ(options.zIndex)
		

		this.render();

		//在事件中心注册
		EventCenter_EventCenter.regist(this,[EventType.COORDINATECHANGE,EventType.INTRACTIVE],this);

		setTimeout(function(){that.checkMousePinter()},500);
		
		
		
	}

	/*获取当前层级标志
	  return 0-在有效范围内（非最值） 1-最大值 -1 最小值 ,-2 层级开关未打开
	*/
	zIndexFlag(){
		if(this.zIndex==-1){
			return -2;
		}
		if(this.zIndex==this.maxZ){
			return 1;
		}else if(this.zIndex==this.minZ){
			return -1;
		}else{
			return 0;
		}
	}

	setDataSource(dataSource){
		this.gridDataCenter.setDataSource(dataSource);
	}

	render(){
		//按坐标视图中心计算偏移


		let pxPerUnitWidth=this.coordinateSystem.pxPerUnitWidth; //单位长的像素距离
		let pxPerUnitHeight=this.coordinateSystem.pxPerUnitHeight;//单位高的像素距离
		//计算可视区域的线
		let centerpxPosition=this.coordinateSystem.getViewCenter();

		let canvasWidth=this.canvas.width;
		let canvasHeight=this.canvas.height;

		//计算偏移量
		let offset=this.calculateOffsetByCoordinateCenter(centerpxPosition,pxPerUnitWidth,pxPerUnitHeight,canvasWidth,canvasHeight);

		//根据偏移量画图
		this.draw.cleanGrid();
		this.draw.drawGrid(offset[0],offset[1],pxPerUnitWidth,pxPerUnitHeight);

		//根据数据填充网格
		let coordExtend=this.coordinateSystem.getViewCoordExtend(this.canvas);
		let unitDatas=this.gridDataCenter.getExtendData(new CoordPosition(coordExtend[0],coordExtend[1]),new CoordPosition(coordExtend[2],coordExtend[3]))
		//将unitDatas 的坐标换算成viewPxPosition
		this._fillRenderDataByViewPxPosition(unitDatas);
		this.draw.fillGrid(unitDatas); //填充格子颜色

		this.draw.fillText(unitDatas); //填充文字
		


	}

	/*对网格数据进行必要设置
	如设置网格的左上角坐标点
	*/
	_fillRenderDataByViewPxPosition(unitDatas){
		for(let i=0;i<unitDatas.length;i++){
			let item=unitDatas[i];
			let viewPXPositionExtend=this.coordinateSystem.getViewPxPositionExtendFromCoordinate(item.x,item.y,this.canvas);
			
			//找到网格的左上角
			item.pxX=Math.floor(viewPXPositionExtend[0].x);
			item.pxY=Math.floor(viewPXPositionExtend[1].y);
			
			item.pxWidth=Math.floor(viewPXPositionExtend[1].x-viewPXPositionExtend[0].x);
			item.pxHeight=Math.floor(viewPXPositionExtend[1].y-viewPXPositionExtend[0].y);
			item.color=item.data.color;


		}
		return unitDatas;
	}


	listenerCall(eventIn){
		let that=this;
		//console.log("gridLayer收到事件"+eventIn.type)
			/*event.eventData={
				"action":"click", //单击
				"startPosition":that._mouseDownPosition			//拖拽起始点
			};*/
		if(eventIn.type==EventType.INTRACTIVE){
			if(!eventIn.eventData){
				return;
			}
			if(!eventIn.eventData.action||eventIn.eventData.action!="click"){
				return;
			}
			let viewPosition={x:eventIn.eventData.startPosition[0],y:eventIn.eventData.startPosition[1]};
			//从视图坐标得到 坐标轴坐标
			let coord=this.coordinateSystem.getCoordFromViewPxPosition(viewPosition);

			//发送控件输出事件
			//发送输出事件
			let event=new Event();
			event.senderObj=this;
			event.type=EventType.OUTPUT;
			event.eventData={
				"action":"clickGrid",   //动作，选择
				"zIndex":that.zIndex, 	//当前层级			
				"coord":coord,          //指针坐标
			};

	    	EventCenter_EventCenter.postEvent(event);
				

		}

	}

	/*根据可视区域中西的在坐标系上的坐标点，计算网格视图的偏移量
	pxPosition-视图中心在坐标系中的像素定位
	pxPerUnitWidth- 单位宽的像素距离（坐标系中）
	pxPerUnitHeight- 单位高的像素距离（坐标系中）
	canvasWidth- 画布像素宽
	canvasHeight-画布像素高
	*/
	calculateOffsetByCoordinateCenter(pxPosition,pxPerUnitWidth,pxPerUnitHeight,canvasWidth,canvasHeight){
		let startX=pxPosition.x -(canvasWidth/2);
		let startY=pxPosition.y-(canvasHeight/2);

		let offsetX=0;
		let offsetY=0;
		if(startX>0){
			 offsetX=pxPerUnitWidth-startX%pxPerUnitWidth;
		}else{
			 offsetX=0-startX%pxPerUnitWidth
		}

		if(startY>0){
			 offsetY=pxPerUnitHeight-startY%pxPerUnitHeight;
		}else{
			 offsetY=0-startY%pxPerUnitHeight;
		}
		
		
		if(offsetX==pxPerUnitWidth){
			offsetX=0;
		}
		if(offsetY==pxPerUnitHeight){
			offsetY=0;
		}
		return [offsetX,offsetY]
	}

	/*视图移动
	viewMoveDistance-[xdistance,ydistance]
	
	*/
	moveView(viewMoveDistance){
		if(viewMoveDistance[0]==0&&viewMoveDistance[1]==0){
			return;
		}
		let that=this;
		let canvaseDistance=viewMoveDistance;
	 	let viewDistance=that.coordinateSystem.translateDistanceFromCanvasXY(canvaseDistance);
	 	that.coordinateSystem.moveViewCenter(viewDistance[0],viewDistance[1]);
		that.render();

	}

	/*视图选中
	*/
	selectView(startCanvasViewPxPosition,endCanvasViewPxPosition){
		

		//操作驱动数据改变(待定，最优方法应当如此)
		let that=this;
		let minX=startCanvasViewPxPosition.x;
		let maxX=endCanvasViewPxPosition.x;
		let minY=startCanvasViewPxPosition.y;
		let maxY=endCanvasViewPxPosition.y;
		if(startCanvasViewPxPosition.x>endCanvasViewPxPosition.x){
			minX=endCanvasViewPxPosition.x;
			maxX=startCanvasViewPxPosition.x;
		}

		if(startCanvasViewPxPosition.y>endCanvasViewPxPosition.y){
			minY=endCanvasViewPxPosition.y;
			maxY=startCanvasViewPxPosition.y;
		}

		
		
		//寻找画图的起点-左上角
		let leftTop=new ViewPXPosition(minX,minY);
		let leftTopViewPxPosition=that.coordinateSystem.translateFromCanvasXY(leftTop,this.canvas);

		let width=maxX-minX;
		let height=maxY-minY;
		//画出选择区域
		this.render();
		this.draw.drawSelected(leftTopViewPxPosition,width,height); //画出选择区域


		//画出选择区域的 左下角和右上角坐标
		let leftBottom=new ViewPXPosition(minX,maxY);
		let rightTop=new  ViewPXPosition(maxX,minY);
		
		let leftBottomViewPxPosition=that.coordinateSystem.translateFromCanvasXY(leftBottom,this.canvas);
		let rightTopViewPxPosition=that.coordinateSystem.translateFromCanvasXY(rightTop,this.canvas);

		//计算像素区域的坐标
		let start=that.coordinateSystem.translateFromCanvasXY(startCanvasViewPxPosition,this.canvas);
		let end=that.coordinateSystem.translateFromCanvasXY(endCanvasViewPxPosition,this.canvas);
		let coordExtend=that.coordinateSystem.getCoordinateExtendByViewExtend(start,end)

		let txt1=coordExtend[2]+","+coordExtend[3]; //右上角数字
		let txt2=coordExtend[0]+","+coordExtend[1]; //左下角数字
		
		this.draw.drawSelectText(leftBottomViewPxPosition,rightTopViewPxPosition,txt2,txt1,this.canvas.width,this.canvas.height);

		//发送输出事件
		let event=new Event();
		event.senderObj=this;
		event.type=EventType.OUTPUT;
		event.eventData={
				"action":"select", //动作，选择
				"extent":coordExtend, 				//坐标范围[x1,y1,x2,y2]
			};

	    	EventCenter_EventCenter.postEvent(event);
	}

	selectGrid(startCanvasViewPxPosition){
		let start=this.coordinateSystem.translateFromCanvasXY(startCanvasViewPxPosition,this.canvas);
		let coordPosition=this.coordinateSystem.getCoordinateFromViewPxPosition(start);

		let txt=coordPosition.x+","+coordPosition.y; //左下角数字
		this.render();
		this.draw.drawGridText(start,txt,this.canvas.width,this.canvas.height);
	}


	/*以鼠标中心点为锚点缩放视图
		return 0- 缩放有效范围  1-超出最大缩放比例（像素最大） -1 超出最小缩放比例（像素最小）
	*/
	scaleView(startCanvasViewPxPosition,scaleSizedelta){


		if(!startCanvasViewPxPosition){
			//已左下角为锚点缩放
			startCanvasViewPxPosition=new ViewPXPosition(0,this.canvas.height);
		}
		let start=this.coordinateSystem.translateFromCanvasXY(startCanvasViewPxPosition,this.canvas);
		let flag=this.coordinateSystem.scaleSystem(start,scaleSizedelta);
		this.render();
		return flag;

		//检查样式改变
		let that=this
		setTimeout(function(){that.checkMousePinter()},500);
	}


	/*

	  对层级进行变化
	  如果层级变化有效，返回0
	  超出最大maxZ，返回1，超出最小minZ，返回-1
	*/
	scaleZ(pxStartCoord,scaleSizedelta){
		
		
		let z=this.zIndex+scaleSizedelta;
		if(z<=this.maxZ&&z>=this.minZ){
			//有效范围
			this.setZ(z);

			//层级转换后需要重新设置视图中心点，否则缩放效果 如同以左下角会锚点缩放
			//层级缩放始终已视图中心为缩放锚点
			this.coordinateSystem.scaleLevel(pxStartCoord,scaleSizedelta);
			this.render();

			//检查样式改变
			let that=this;
			setTimeout(function(){that.checkMousePinter()},500);
			return 0;
		}else{
			if(z>this.maxZ){
				return 1;
			}else{
				return -1;
			}
		}
	}


	/*设置Z 层级
	设置后需要调用render 才会看到该效果，如果不调用，则试图不会刷新
	*/
	setZ(zIndex){
		this.zIndex=zIndex;
		this.gridDataCenter.setZIndex(this.zIndex)
	}

	/*设置中心坐标*/
	locationView(coordPosition){
		this.coordinateSystem.setCoordCenter(coordPosition.x,coordPosition.y);
		this.render();
	}

	/*设置中心像素坐标*/
	locationViewPxPosition(coordPxPosition){
		this.coordinateSystem.setViewCenter(coordPxPosition.x,coordPxPosition.y);
		this.render();
	}

	//检查鼠标是否需要变换样式
	checkMousePinter(){
		//console.log("检查鼠标样式")

		//发送鼠表css事件
		let event=new Event();
		event.senderObj=this;
		event.type=EventType.POINTCSS;

		//鼠标指针在最大层级时需要变化为手样式
		if(this.zIndex==this.maxZ){
			event.eventData={
					"action":"cssChange",    //样式改变
					"cursorStyle":"pointer", //手指模式
				};
		}else{
			event.eventData={
					"action":"cssChange",    //样式改变
					"cursorStyle":"default", //默认指针模式
				};
		}
		EventCenter_EventCenter.postEvent(event);
	}


}


// CONCATENATED MODULE: ./src/js/MFGrid.js









class MFGrid_MFGrid{ 
	

	constructor(options) {
		

		let viewContainer=options.viewContainer;
		this._viewContainer=viewContainer;
		EventCenter_EventCenter.preInit(new EventCenter_EventCenter());
		//注册事件中心
	  	EventCenter_EventCenter.regist(this,[EventType.ALL])

        //初始网格层

        this._gridView = document.createElement('canvas');
	    this._gridView.style.position = 'absolute';
	    this._gridView.style.zIndex = '100';
	    this._gridView.style.width = '100%';
	    this._gridView.style.height = '100%';
	    this._viewContainer.appendChild(this._gridView);
	    //必须设置canvas的width 和height，css仅设置宽高会引起画面模糊
	     this._gridView.width=viewContainer.clientWidth;
	     this._gridView.height= viewContainer.clientHeight;

		let options1={};
	    options1.canvas=this._gridView;
	    

	    //设置数据源
	    if(options.dataSource){
	    	  options1.dataSource=options.dataSource;
	    	  options1.minZ= options.minZ;
	    	  options1.maxZ=options.maxZ;
	    	  options1.zIndex= options.zIndex;
	    }
	    this.gridLayer=new GridLayer_GridLayer(options1);
	    console.log("gridLayer 初始化完成")
	  	this.gridLayer.render();//初始化后调用一次渲染，防止数据源延迟初始化


	    //初始化交互视图层
	    this._intractiveView = document.createElement('div');
	    this._intractiveView.style.position = 'absolute';
	    this._intractiveView.style.zIndex = '200';
	    this._intractiveView.style.width = '100%';
	    this._intractiveView.style.height = '100%';
	     this._intractiveView.style.opacity=1;
	    this._viewContainer.appendChild(this._intractiveView);
	    let options2={};
	    
	    options2.viewEle=this._intractiveView;
	     //options2.viewEle=this._gridView
	 	this.intractiveLayer=new IntractiveLayer_IntractiveLayer( options2);



    }

	

	/*设置操作模式

	*/
	setOperationMode(mode){
		this.intractiveLayer.setOperationMode(mode);
	}


	/*按事件中心EventCenter规范实现该方法*/
	listenerCall(event){
		let that=this;
		switch(event.type) {

		     case EventType.INTRACTIVE:  //交互事件
		         that.handerIntractive(event);
				 break;
			case EventType.OUTPUT:  	//组件输出事件
		    	 that.handleOutput(event);
		    	 break;
		    case EventType.DATAREFRESH:  	//数据源更新事件
		    	 that.handleDatasourceRefresh(event);
		    	 break;
		     default:
		       // 默认代码块
		} 

	}


	handerIntractive(event){
		let operateMode=this.intractiveLayer.operationMode;
		let that=this;
		switch(event.eventData.action){

			case "drag": //拖拽事件
	 			if(operateMode==OperationMode.MOVEMODE){ //拖拽移动
	 				
	 				that.gridLayer.moveView(event.eventData.moveDistance);

	 			}else if(operateMode==OperationMode.SELECTMODE){  //拖住选择

	 				let startViewPxPosition=new ViewPXPosition(event.eventData.startPosition[0],event.eventData.startPosition[1]);
	 				let endViewPxPosition=new ViewPXPosition(event.eventData.movePosition[0],event.eventData.movePosition[1]);

	 				that.gridLayer.selectView(startViewPxPosition,endViewPxPosition);
	 			}
	 		

	 		break;

	 		case "click"://单击事件
	 			if(operateMode==OperationMode.SELECTMODE){  //单击选择

	 				let startViewPxPosition=new ViewPXPosition(event.eventData.startPosition[0],event.eventData.startPosition[1]);

	 				that.gridLayer.selectGrid(startViewPxPosition);
	 			}
	 		break;


	 		case "scale"://缩放事件

	 				/*缩放逻辑
	 				层级开关开放
						1-先缩放层级
							层级缩放到最大（展示数据少）时，开始缩放视图（慢慢变大）	 展示更详细的数据

						2- 视图缩放到最小时（展示数据最多），开时缩放层级（数据展示变多）  展示更多数据
					
					层级开关不开放
						直接缩放视图
					*/
	 				let scaleSizedelta=event.eventData.scaleSizedelta;
	 				if(!event.eventData.startPosition){
	 					console.log("防止出错")
	 					console.log(event)
	 				}
	 				
	 				let startViewPxPosition=new ViewPXPosition(event.eventData.startPosition[0],event.eventData.startPosition[1]);



					let zIndexFlag=that.gridLayer.zIndexFlag();
					//------------------层级开关关闭
					if(zIndexFlag==-2){
						that.gridLayer.scaleView(startViewPxPosition,scaleSizedelta);
					}
					//------------------层级开关打开
					//放大操作
					if(scaleSizedelta>0&&zIndexFlag!=-2){
						if(zIndexFlag==1){ //最大层级，开始放大视图
							let scaleViewFlag=that.gridLayer.scaleView(startViewPxPosition,scaleSizedelta);
							//console.log("放大视图")
						}else{
							let scaleZFlag=that.gridLayer.scaleZ(startViewPxPosition,scaleSizedelta); //继续放大层级
							//console.log("放大层级")
						}
					}

					//缩小操作
					if(scaleSizedelta<0&&zIndexFlag!=-2){
						
						if(zIndexFlag==1){
							//尝试先缩小视图
							let scaleViewFlag=that.gridLayer.scaleView(startViewPxPosition,scaleSizedelta);
								//console.log("缩小视图")
							if(scaleViewFlag==-1){ //视图缩小到极限，开始缩小层级
								that.gridLayer.scaleZ(startViewPxPosition,scaleSizedelta); 
								//console.log("缩小视图到极限-开始缩小层级")
							}
						}else if(zIndexFlag==-1){ //最小层级，无法继续操作
							
						}else{
							let scaleZFlag=that.gridLayer.scaleZ(startViewPxPosition,scaleSizedelta); //继续缩小层级
							//console.log("缩小层级")
						}
					}



	 		break;


	 		case "location"://定位事件
	 				let coordPosition=new CoordPosition(event.eventData.coordPosition[0],event.eventData.coordPosition[1]);
	 				//let scaleSizedelta=event.eventData.scaleSizedelta;
	 				that.gridLayer.locationView(coordPosition);
	 		break;
			}


	}

	
	/*处理数据输出事件
	*/
	handleOutput(event){
		if(this.handlerFunction){
			this.handlerFunction(event);
		}
	}

	

	/*处理数据源更新事件
	*/
	handleDatasourceRefresh(event){
		 console.log("收到数据源更新通知")
		if(this.gridLayer)
		this.gridLayer.render();
	}


    //------------------------外部API----------------------
   /* 在使用api操作数据时可能感知不道数据变化，可以调用该方法刷新视图
   //重置试图层级为最大层级
   */
    refreshView(){
    	this.gridLayer.render();
    }



	/*设置数据输出处理器
	*/
	setOutputHandler(handlerFunction){
		this.handlerFunction=handlerFunction;
	}


	/*移动位置
	* x-x坐标
	  y-y坐标
	*/
	moveCoordPosition(x,y){
		this.gridLayer.locationView({x:x,y:y})
	}

	/*根据坐标获取数据源中的值
	coordX-x坐标
	coordY-y坐标
	*/
	getGridDataByCoord(coordX,coordY){
		return this.gridLayer.gridDataCenter.getUnitData(coordX,coordY);
	}

	/*设置数据源中的值
	*/
	setUnitData(unitData){
		 this.gridLayer.gridDataCenter.setUnitData(data);
	}


	//-------------------
	/*打开层级开关（该开关控制数据源的获取方式）
		minZ-最小层级
		maxZ-最大层级
	*/
	openZ(z,minZ,maxZ){

	}

	closeZ(){

	}

}



/***/ })
/******/ ]);
});
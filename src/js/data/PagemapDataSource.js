
import { UnitData,BlockData } from './RenderData.js'
class PagemapDataSource{

	constructor(){

	}

	get(x,y,z,callback){
		if(x%2==0){
			data.color="#f0f";
		}else{
			data.color="#000";
		}
		let unitData=new UnitData(x,y,data);
		
		callback(data);
	}

	/*获取块数据
	scaleX-偏移倍率
	scaleY-偏移倍率
	callback-获取成功后回调
	return-unitData的数组形式
	*/
	getBlock(scaleX,scaleY,callback){

		let unitDatas=[];
		if(scaleX==1&&scaleY==1){

			unitDatas.push(new UnitData(1,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(2,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(3,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(4,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(5,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(6,1,{"color":"#87CEFA"}))



		}else{

		}
		callback(unitDatas);
	}


	/*获取块数据(支持Z)
	scaleX-偏移倍率
	scaleY-偏移倍率
	callback-获取成功后回调
	return-unitData的数组形式
	*/
	getBlockZ(scaleX,scaleY,z,callback){

		let unitDatas=[];
		if(scaleX==1&&scaleY==1){

			unitDatas.push(new UnitData(1,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(2,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(3,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(4,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(5,1,{"color":"#87CEFA"}))
			unitDatas.push(new UnitData(6,1,{"color":"#87CEFA"}))



		}else{

		}
		callback(unitDatas);
	}

}

export {PagemapDataSource};


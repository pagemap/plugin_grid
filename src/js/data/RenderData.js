

import {EventCenter,Event} from '../event/EventCenter.js';
import {EventType} from '../event/event.js';

class GridDataCenter{

	constructor(dataSource){
		EventCenter.regist(this,[EventType.NONE]);

		this.blockDatas={}; //{ "xScale_yScale":blockData }
		
		//数据源因为通用 所以绑定到类上
		GridDataCenter.dataSource=dataSource;

		//设置Z层级
		this.zIndex=-1; //-1表示初始化值，并且不启用

		this.scaleSize=100; //设置区块大小 表示横竖为100的单元格数量 最大1w个数据（需要改变）
		
	}

	/*设置数据源的层级*/
	setZIndex(zIndex){
		if(!zIndex){
			console.log("z轴开启无效")
			//设置Z层级
			return;
		}
		if(this.zIndex!=zIndex){
			//因为不会缓存层级数据，所以每次切换都需要清空原来数据
			this.blockDatas={};
			this.zIndex=zIndex;
		}
	}



	//重新设置dataSource
	setDataSource(dataSource){
		GridDataCenter.dataSource=dataSource;
	}

	setUnitData(unitData){
		//计算偏移倍数
		let scaleNum=BlockData.calculateScale(unitData.x,unitData.y);
		let key=scaleNum[0]+"_"+scaleNum[1];
		

		//将数据添加到分片中
		let targetBlockData=this.blockDatas[key];
		
		if(!targetBlockData)
		{
			targetBlockData=new BlockData(this.zIndex,scaleNum[0],scaleNum[1]);
			this.blockDatas[key]=targetBlockData;
		}
		targetBlockData.set(unitData);
		this.onDataChange();
	}

	/*获取区域范围内的渲染数据
	startCoordinatePosition-指定区域的起始坐标
	endCoordinatePosition-指定区域的结束坐标
	return-[unitData1,unitData2] 数据数组
	*/
	getExtendData(startCoordinatePosition,endCoordinatePosition){
		
		let unitDataArray=[];
		//计算区域
		let scales=BlockData.calculateScaleExtend(startCoordinatePosition,endCoordinatePosition);
		for(let i=0;i<scales.length;i++){
			let scaleNum=scales[i];
			
			//获取区块数据
			let targetBlockData=this.getAndsetInNotExistBlockDataByScale(scaleNum[0],scaleNum[1]);
			
			let blockAllData=targetBlockData.getAll();
			//过滤掉不在指定范围内的数据
			for(let ii=0;ii<blockAllData.length;ii++){
				let unitData=blockAllData[ii];
				
				//判断数据是否在指定坐标区域内
				if(unitData.x>=startCoordinatePosition.x&&unitData.x<=endCoordinatePosition.x){
					if(unitData.x>=startCoordinatePosition.x&&unitData.x<=endCoordinatePosition.x){
						//符合要求
						unitDataArray.push(unitData);
					}
				}
			}

		}

		//返回区域的所有数据
		return unitDataArray;
	}

	/*获取单个数据
	x-x坐标
	y-y坐标
	return-unitData
	*/
	getUnitData(x,y){
		//计算偏移倍数
		let scaleNum=BlockData.calculateScale(x,y);

		let targetBlockData=this.getBlockDataByScale(scaleNum[0],scaleNum[1]);
		
		if(!targetBlockData){
			this.blockDatas[key]=new BlockData(this.zIndex,scaleNum[0],scaleNum[1]);
			targetBlockData=this.blockDatas[key];
		}
		//交由底层的分片块 去获取数据，从缓存获取，或者让从数据源获取
		return targetBlockData.get(x,y);
		
	}

	/*获取指定倍率的blockData
	xScale-x倍率
	yScale-y倍率
	*/
	getBlockDataByScale(xScale,yScale){
		let key=xScale+"_"+yScale;
		//将数据添加到分片中
		let targetBlockData=this.blockDatas[key];
		return targetBlockData;
	}

	/*获取blockdata，如果不存在则初始化
	xScale-x倍率
	yScale-y倍率
	return-blockData
	*/
	getAndsetInNotExistBlockDataByScale(xScale,yScale){
		let key=xScale+"_"+yScale;

		//如果不存在则，将数据设置进去
		if(!this.blockDatas[key]){
			this.blockDatas[key]=new BlockData(this.zIndex,xScale,yScale,this.onDataChange());
		}
		return  this.blockDatas[key];
	}

	onDataChange(){
			let that=this;
			return function(){
				console.log("onDataChange 数据源有变化");
				let event=new Event();
		    	event.senderObj=that;
				event.type=EventType.DATAREFRESH;

				event.eventData={
					"action":"refresh", //动作，刷新数据
				};

		    	EventCenter.postEvent(event);
			}
			

	}

}



class UnitData{

	constructor(x,y,data){

		this.x=x;
		this.y=y;
		this.data=data;
	}
}

//将100*100的网格划分一个块
class BlockData{
	/*构造函数
	xScale-区块x轴偏移量
	yScale-区块y轴偏移量
	notifyFunction-区块数据变动通知方法*/

	//20210107 添加对层级数据的支持
	constructor(zIndex,xScale,yScale,notifyFunction){
		//区块的偏移倍数，如坐标在[0,0]-[99,99] 则偏移倍数为 （0，0）
		//如坐标在[100,100]-[199,199] 则偏移倍数为 （1，1）
		this.xScale=xScale;
		this.yScale=yScale;
		this.zIndex=zIndex; //zIndex=-1 ,默认使用getBlock方法
		this.arr = new Array(100);

		//初始化数据块
		this.refreshData(); 
		this.notifyFunction=notifyFunction;
		
	}

	

	/*刷新数据块内的数据

	*/
	refreshData(){
		let that=this;
		//从数据源获取
		//20210107 支持层级数据源获取
		
		GridDataCenter.dataSource.getBlock(that.zIndex,this.xScale,this.yScale,function(datas){
			for(let i=0;i<datas.length;i++){
				let unitData=datas[i];
				that.set(unitData);
			}
			//通知数据有变动
			if(that.notifyFunction){
				that.notifyFunction();
			}
		
		});
		
	}

	set(unitData){
		//计算数组中x y坐标
		let arrayXY=this.calculateArrayPositionByCoord(unitData.x,unitData.y);
		let x=arrayXY[0];
		let y=arrayXY[1];

		if(this.arr[x]){
			this.arr[x][y]=unitData;
		}else{
			this.arr[x]=new Array(100);
			this.arr[x][y]=unitData;
		}
	}

	get(coordx,coordy){
		let arrayXY=this.calculateArrayPositionByCoord(coordx,coordy);
		let x=arrayXY[0];
		let y=arrayXY[1];
		if(!this.arr[x]){
			return;
		}
		let unitData=this.arr[x][y];
		//如果未获取到数据，则从数据源获取(通过刷新动作重新获取数据)
		if(!unitData){
			/*let z=17
			unitData=GridDataCenter.dataSource.get(x,y,z);
			//保存数据
			this.setUnitData(unitData);*/
		}
		return unitData;
	}

	//获取区块当前所有数据
	getAll(){
		//flat 不能处理负坐标问题
		//let newArray = this.arr.flat(1);
		let newArray=new Array();
		for(let i in this.arr){
			if(this.arr[i]){
				for(let ii in this.arr[i]){
					if(this.arr[i][ii]){
						newArray.push(this.arr[i][ii])
					}
				}
			}
		}
		return newArray;
	}

	/*根据坐标计算在blockdata 中的二维数组位置
	x-x坐标
	y-y坐标
	return-[x,y]在blockdata中的二维数组位置
	*/
	calculateArrayPositionByCoord(x,y){
		//因为 100*100的区域
		return [x % 100,y % 100 ];
	}

}

/*对坐标计算偏移倍数
*/
 BlockData.calculateScale=function(xPosition,yPosition){
 	return [Math.floor(xPosition/100),Math.floor(yPosition/100)];
}

BlockData.calculateScaleExtend=function(startCoordinatePosition,endCoordinatePosition){
	let startScale=BlockData.calculateScale(startCoordinatePosition.x,startCoordinatePosition.y);
	let endScale=BlockData.calculateScale(endCoordinatePosition.x,endCoordinatePosition.y);

	let scales=[];
	for(let i=startScale[0];i<=endScale[0];i++){
		for(let ii=startScale[1];ii<=endScale[1];ii++){

			scales.push([i,ii]);
		}
	}
	return scales;
}


export {GridDataCenter,BlockData,UnitData};
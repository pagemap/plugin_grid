
let uuid= function () {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
        d += performance.now(); //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}


let sumOffset=function(ele){
        

    let parentEle=ele.offsetParent;
    let offsetLeft=ele.offsetLeft;
    let offsetTop=ele.offsetTop;
    while(true){

        //如果父偏移对象不存在，则返回
        if(!parentEle){
            return [offsetLeft,offsetTop];
        }
        //对偏移量累加
        offsetLeft+=parentEle.offsetLeft;
        offsetTop+=parentEle.offsetTop;
        
       parentEle=parentEle.offsetParent;
    }

   

}


let loadFile=function(filename, fileType){
    if(fileType == "js"){
        var oJs = document.createElement('script');
        oJs.setAttribute("type","text/javascript");
        oJs.setAttribute("src", filename);//文件的地址 ,可为bai绝对及相对路径du
        document.getElementsByTagName("head")[0].appendChild(oJs);//绑定
    }else if(fileType == "css"){
        var oCss = document.createElement("link");
        oCss.setAttribute("rel", "stylesheet");
        oCss.setAttribute("type", "text/css");
        oCss.setAttribute("href", filename);
        document.getElementsByTagName("head")[0].appendChild(oCss);//绑定
    }
}


let loadStyle=function(cssString){
    var doc=document;
    var style=doc.createElement("style");
    style.setAttribute("type", "text/css");
 
    if(style.styleSheet){// IE
        style.styleSheet.cssText = cssString;
    } else {// w3c
        var cssText = doc.createTextNode(cssString);
        style.appendChild(cssText);
    }
 
    var heads = doc.getElementsByTagName("head");
    if(heads.length)
        heads[0].appendChild(style);
    else
        doc.documentElement.appendChild(style);
}


export {uuid,sumOffset,loadFile,loadStyle};
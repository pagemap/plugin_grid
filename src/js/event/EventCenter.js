import {EventType} from "./event.js"
import {uuid} from "../util.js";

class EventCenter{
	

	constructor(){
		this.listeners=[];
		this.eventListenerMap={}; //{ "eventType1":[listener1,listener2]}
	}

	/*在事件中心注册客户端
	obj-注册客户端对象
	eventTypes-事件类型 数组[]
	call-事件的处理方法
	*/
	regist(obj,eventTypes,call){
		let listener=new Lisenter(obj,eventTypes,call);
		this.listeners.push(listener);

		if(this.checkIfEventTypesValiable(eventTypes)){
			
			if(EventType.ALL in eventTypes && eventTypes.length!=1){
				throw "监听器 EventType.ALL 类型不能与其他类型一起使用";
				return ;
			}

			for(let i=0;i<eventTypes.length;i++){
				let eventType=eventTypes[i];

				if(eventType in this.eventListenerMap){
					this.eventListenerMap[eventType].push(listener);
				}else{
					this.eventListenerMap[eventType]=[listener];
				}

			}

		}

		

	}

	/*发送事件消息
	event-事件，对象-js/event/EventCenter.js-Event
	*/
	postEvent(event){
		//检查是否在
		if(!this.checkIfRegistered(event.senderObj)){
			throw "对象未在 事件中心注册，不允许发送消息,"+event.senderObj.constructor.name;
			return;
		}

		//循环调用事件的处理逻辑
		let eventType=event.type;

		let listnerArray=this.getListenersByEventType(eventType);
		for(let i=0;i<listnerArray.length;i++){
			listnerArray[i].call(event);
		}

		//对all类型通知
		 eventType=EventType.ALL;
		 listnerArray=this.getListenersByEventType(eventType);
		for(let i=0;i<listnerArray.length;i++){
			listnerArray[i].call(event);
		}
		
	}

	/*检查发送者是否在事件中心注册过
	  只有注册过的对象才能处理接收和发送事件
	  senderObj-消息源对象
	*/
	checkIfRegistered(senderObj){
		for(let i=0;i<this.listeners.length;i++){
			if(senderObj.eventCenterId==this.listeners[i].getId()){
				return true;
			}
		}
		return false;
		
	}

	/*检查事件类型是否符合要求
	eventTypes-事件类型数组（字符串的数组）
	return- true-符合要求 false-不符合要求
	*/
	checkIfEventTypesValiable(eventTypes){
		for(let i=0;i<eventTypes.length;i++){
			if(! (eventTypes[i] in EventType)){
				return false;
			}
		}
		return true;
	}


	/*根据事件类型获取监听者
	eventType-事件类型
	return-Listener数组
	*/
	getListenersByEventType(eventType){
		if(eventType in this.eventListenerMap){
			let listnerArray=this.eventListenerMap[eventType]
			return listnerArray;
		}else{
			return [];
		}
	}


}

//----------------将事件中心的某些方法设置成全局

/*事件中心的获取*/
EventCenter.instance=function(){
	
	if("eventCenter" in window){
		return window.eventCenter;
	}else{
		throw "EventCenter 对象未初始化"
	}
	
}

/*事件中心绑定到window对象，使其成为全局对象*/
EventCenter.preInit=function(eventCenter){
	if("eventCenter" in window){
		//throw "EventCenter 不需要重复初始化" ;
		return;
	}
	 window.eventCenter=eventCenter;
}


EventCenter.regist=function(obj,eventTypes,call){
	EventCenter.instance().regist(obj,eventTypes,call);
}

EventCenter.postEvent=function(event){
	EventCenter.instance().postEvent(event);
}


class Lisenter{

	/*构造函数
	obj-注册者
	eventTypes-监听的事件类型[type1,type2]
	call-监听者对象*/

	constructor(obj,evenTypes,call){
		this._obj=obj;
		this._types=evenTypes;
		this._id=uuid();
		this._obj.eventCenterId=this._id;
		
		//监听对象call 如果为空则，将obj设置为监听者，监听者必须实现listenerCall 方法
		if(!call){
			this._call=obj;
		}else{
			this._call=call;	
		}
		

	}

	getId(){
		return this._id;
	}
	call(event){
		this._call.listenerCall(event);
	}

}

class Event{
	constructor(senderObj,eventType,eventData){
		this.senderObj=senderObj;
		this.eventType=eventType;
		this.eventData=eventData;
	}
}

export {EventCenter,Event,Lisenter};
let EventType={
	"ALL":"ALL",  //表示监听所有事件
	"NONE":"NONE",//空类型，该类型的事件不会转发任何监听者
	"INTRACTIVE":"INTRACTIVE", //交互事件
	"COORDINATECHANGE":"COORDINATECHANGE", //坐标系统改变（所有的视图改变都由该事件触发。）

	"OUTPUT":"OUTPUT" ,//控件数据输出事件
	"DATAREFRESH":"DATAREFRESH", // 数据源刷新事件

	"POINTCSS":"POINTCSS", //鼠标样式事件
};




export  {EventType};

import {EventCenter,Event} from '../event/EventCenter.js';
import {EventType} from '../event/event.js';
import {sumOffset} from '../util.js'


class Selectable{

	constructor(intractiveLayer){

		//鼠标状态 1-按下 0-弹起 
		this._mouseUpStatus=0;
		//鼠标移入状态1-移入  0-移出
		this._mouseInStauts=0;

		this.intractiveLayer=intractiveLayer;
		EventCenter.regist(this,[EventType.NONE]);

		//移动状态 0-非移动  1-移动
		this._movingStatus=0;


	}

	
	/* 鼠标点击逻辑
	mouseEvent-鼠标事件
   */
    onmousedown(mouseEvent){
    	this._movingStatus=0;
    	//开始选择
    	this._mouseUpStatus=1;

    	let offsetXY=this.intractiveLayer.getViewOffset();
    	//let x=mouseEvent.clientX-offsetXY[0];
    	//let y=mouseEvent.clientY-offsetXY[1];

    	let x=mouseEvent.offsetX;
    	let y=mouseEvent.offsetY;
    	
    	//鼠标按下位置
    	this._mouseDownPosition=[x,y];
    	//鼠标上次移动到的位置
    	this._mouseLastDragPosition=[x,y];
    }

	/* 鼠标点击放开逻辑
	mouseEvent-鼠标事件
   */
    onmouseup(mouseEvent){
    	//选择完成
    	this._mouseUpStatus=0;
    	

    }


    onmouseover(mouseEvent){
    	this._mouseInStauts=1;
    }

    onmouseout(mouseEvent){
    	this._mouseInStauts=0;
    	this.onmouseup(mouseEvent); //移出视图 ，也当做选择完成
    }

    onclick(mouseEvent){
    	//如果鼠标处于移动事件，则不触发点击事件
    	if(this._movingStatus==1){
    		return
    	}
    	let event=new Event();
    	let that=this;
    	event.senderObj=this;
		event.type=EventType.INTRACTIVE;

    	event.eventData={
				"action":"click", //单击
				"startPosition":that._mouseDownPosition			//拖拽起始点
			};
		EventCenter.postEvent(event);
    }

	/* 鼠标移动逻辑
	mouseEvent-鼠标事件
	return
   */
    onmousemove(mouseEvent){
    	if(this._mouseUpStatus==1&&this._mouseInStauts==1){
	    		//鼠标按下并移动--拖动操作
	    	this._movingStatus=1;	

	    	let event=new Event();
	    	event.senderObj=this;
			event.type=EventType.INTRACTIVE;

			let that=this;


			//clientX 和clientY 要减去屏幕偏移位置 offsetTop,
			let offsetXY=this.intractiveLayer.getViewOffset();
	    	//let x=mouseEvent.clientX-offsetXY[0];
	    	//let y=mouseEvent.clientY-offsetXY[1];

	    	let x=mouseEvent.offsetX;
    	    let y=mouseEvent.offsetY;

			let currentPosition=[x,y];//当前鼠标移动到的位置

			let moveDistance=[currentPosition[0]-that._mouseLastDragPosition[0],currentPosition[1]-that._mouseLastDragPosition[1]]
			
			event.eventData={
				"action":"drag", //动作，拖拽
				"startPosition":that._mouseDownPosition, 				//拖拽起始点
				"movePosition":currentPosition, 						//拖拽到达点
				"lastPosition":that._mouseLastDragPosition , 			//鼠标上次移动的位置
				"moveDistance":moveDistance
			};

	    	EventCenter.postEvent(event);

	    	//更新上一次鼠标拖动位置为当前位置
	    	this._mouseLastDragPosition=[mouseEvent.clientX,mouseEvent.clientY];
    	}else{
    		//鼠标弹起-鼠标移动操作
    		//doNothing
    	}
    }


}

export {Selectable};
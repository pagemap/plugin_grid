
import {EventCenter,Event} from '../event/EventCenter.js';
import {EventType} from '../event/event.js';
import {loadFile,loadStyle} from '../util.js';
import {OperationMode} from '../layer/IntractiveLayer.js';


/*模式切换控制面板*/
class QuickLocationPan{

	constructor(intractiveLayer){

		

		this.intractiveLayer=intractiveLayer;
		this.mode=intractiveLayer.operationMode;
	
		
		this._initStyle();
		this._initPan();
			//事件中心注册
		EventCenter.regist(this,[EventType.NONE],null);
	}


	_initPan(){
		let that=this;
		
		//加载面板元素
		let pandiv=document.createElement("div");
		pandiv.style="float:left;height:30px;margin-top:10px;margin-left:10px;border:1px solid #808080;";
		

		//<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAWbSURBVHjapFZvaBtlGP/d5ZJr7po2Tdqwy7qYdFnXHk3GVkHZB5WVdeyTaP27YmVOdDJEYTphyvzzccoEP/ptwvw3Vv9Ct8ksdNmko2vBtHUpmyGjoUmapM1yd7k/uff8spRsiQr6wAP3vs9z93t+z/s8z3vU/Pw8OI5DOp3G0aNHYRgGAoFAJJ1OP7G8vPxYsVgMm6bpbGlpybvd7oW+vr6fVFW9KIriitvtRm9vL3ieByEEfycMAJimCZ7nwfO8Z2lp6YOFhYXXdF1n6h1lWe6UZbkvnU6PeDyem21tbe8JgvANRVH4N6FZlkUwGMStW7cCN2/ePJ9Op1+/H+B+KRaL4YmJia+vXLnyqcfj+UcWAGDbs2cPLly4IJw8efJ8MpnceY/RZkNXV1fC6XQmATgMw+Dr7clk8mGGYRy7d+/+VVVV0DTdVJlisYjTp0+/s7S0NFB7maIohMPhcZfL9Ukmk0nSNK273W5XKBR6dHZ29t1CodBb8z1z5sxxp9MZC4fDE3fu3EGz9NGZTGYgHo+/Ub/pcrk+pCjqGZfL9ZumaRnDMIo0Tad4nv8iEokM9/f3z9E0veE/PT19pFKp2DRNg6IoDcpMTU2N1Od0cHDw+7a2tg8oioIkSRuRWZaFSqWCSqWS2rdv34uFQuFyLpdrB4BEIrE3k8ns3L59+4wkSY1M5ubmhjcWNG3t3bv3o7GxMQwPD0NRFFiWBcuyoGkaWJYFAKRSqbggCN/W2Oi67sjn8zur1So0TWtQJp/P99RA3G53YnJyMhmLxWAYBhRFAcuyIISgp6cHLMuiq6sLqqqiu7t7YnFx8RAhhAaAxcXF3nK5DFVVG/uEENJSW9jt9mI2myW6rm+kKRQKYdOmTeA4DqZpoq44SgAIAPpupbHpdLppOTMOh2NN13U3AEiSFDpy5IgjEAhAkiSMj4/DMAxwHIcbN26gpaUFHo8HDocDuVwuTAjZ6Kddu3atiaIIWZYbQXw+3/zy8nLoblcL+Xx+D8dx3yqKAqfTCVVVQQjB2toaJEnC4cOHMTAwgOPHjz9Tz2zz5s1/iKKI9fX1xoMfGhoar984e/bsx7FYTLh27RqcTifqigIA4PF4MD8/fzCVSj1Ss4VCobmtW7deLJVKqFarG2qaJgzDABOJRH72er1/FgqFHgBYWVkJTE9PTwwNDY3qur5AURRomgZFUfD5fJicnDx86dKlzzRNs9dAfD7fqiRJxUQicQ8DXdfhcrlgO3HihOJyuVZjsdhIzShJ0qbbt28/tba2JnZ2djpZln2gUCjsX11dfX9mZuZNTdNs9R/L5XJBVVV7W1tbz5fLZaNSqUBVVayvr4NhGFBTU1Ow2Ww4derUyXPnzr19fz7tdjsBQAghTP0ZNJPBwcGvuru7DyqKogGAoijwer2wjY6OQpZldHR0/FIoFFpKpdJDuq5vREoIoQghtGVZ/zrSV1ZWIhzHiV6v9wfDMMxqtQqO42AbGxtDtVpFKpVCa2vrJVmWp1mWba9Wq0HDMBpGfiAQ+F0QhBv5fD7YDCiTyfTzPB/p7Oz8TpZlk+M4MPXVc3eMXAyFQhd5nn/Q4/HsyGaz4Ww26xBFsShJUqK9vf1yMBgs2+32z+Px+GgzoNnZ2cdpmv6yo6PjBQBKQ6SWZaFcLoNhmJlwODxD0zTK5TK2bduGbDaLTCaDUqmELVu2vAyAjsfjzzcDun79+pOiKOrRaPRQ0xuQoigQQqAoCjRNg2maG8+1cVOpVFS/3/8KRVF0PB5/9v4zsywLCwsLz7W3t5do/EexLAuqqkp+v/+lHTt2jP+d39WrV1/9zyB1PxiKIAgHotHoj03vd5st+b9BKIqCoiiaIAjPDgwMNDA6duzYW/8bpAakqqrq9/sPRKPRzwHccTgc8yMjI0/v379//K8BAAT30BUo/WZ0AAAAAElFTkSuQmCC' alt=''>\
			   
		//
		//图片使用base64转码
		 pandiv.innerHTML=
		   "<div class='search' id='mfgrid:quicklocationpan:search'>\
			  <input type='text' name='condition' placeholder='输入坐标' id='mfgrid:quicklocationpan:input'>\
			  <input type='submit' value='搜索' id='mfgrid:quicklocationpan:button'\
			     type='background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAWbSURBVHjapFZvaBtlGP/d5ZJr7po2Tdqwy7qYdFnXHk3GVkHZB5WVdeyTaP27YmVOdDJEYTphyvzzccoEP/ptwvw3Vv9Ct8ksdNmko2vBtHUpmyGjoUmapM1yd7k/uff8spRsiQr6wAP3vs9z93t+z/s8z3vU/Pw8OI5DOp3G0aNHYRgGAoFAJJ1OP7G8vPxYsVgMm6bpbGlpybvd7oW+vr6fVFW9KIriitvtRm9vL3ieByEEfycMAJimCZ7nwfO8Z2lp6YOFhYXXdF1n6h1lWe6UZbkvnU6PeDyem21tbe8JgvANRVH4N6FZlkUwGMStW7cCN2/ePJ9Op1+/H+B+KRaL4YmJia+vXLnyqcfj+UcWAGDbs2cPLly4IJw8efJ8MpnceY/RZkNXV1fC6XQmATgMw+Dr7clk8mGGYRy7d+/+VVVV0DTdVJlisYjTp0+/s7S0NFB7maIohMPhcZfL9Ukmk0nSNK273W5XKBR6dHZ29t1CodBb8z1z5sxxp9MZC4fDE3fu3EGz9NGZTGYgHo+/Ub/pcrk+pCjqGZfL9ZumaRnDMIo0Tad4nv8iEokM9/f3z9E0veE/PT19pFKp2DRNg6IoDcpMTU2N1Od0cHDw+7a2tg8oioIkSRuRWZaFSqWCSqWS2rdv34uFQuFyLpdrB4BEIrE3k8ns3L59+4wkSY1M5ubmhjcWNG3t3bv3o7GxMQwPD0NRFFiWBcuyoGkaWJYFAKRSqbggCN/W2Oi67sjn8zur1So0TWtQJp/P99RA3G53YnJyMhmLxWAYBhRFAcuyIISgp6cHLMuiq6sLqqqiu7t7YnFx8RAhhAaAxcXF3nK5DFVVG/uEENJSW9jt9mI2myW6rm+kKRQKYdOmTeA4DqZpoq44SgAIAPpupbHpdLppOTMOh2NN13U3AEiSFDpy5IgjEAhAkiSMj4/DMAxwHIcbN26gpaUFHo8HDocDuVwuTAjZ6Kddu3atiaIIWZYbQXw+3/zy8nLoblcL+Xx+D8dx3yqKAqfTCVVVQQjB2toaJEnC4cOHMTAwgOPHjz9Tz2zz5s1/iKKI9fX1xoMfGhoar984e/bsx7FYTLh27RqcTifqigIA4PF4MD8/fzCVSj1Ss4VCobmtW7deLJVKqFarG2qaJgzDABOJRH72er1/FgqFHgBYWVkJTE9PTwwNDY3qur5AURRomgZFUfD5fJicnDx86dKlzzRNs9dAfD7fqiRJxUQicQ8DXdfhcrlgO3HihOJyuVZjsdhIzShJ0qbbt28/tba2JnZ2djpZln2gUCjsX11dfX9mZuZNTdNs9R/L5XJBVVV7W1tbz5fLZaNSqUBVVayvr4NhGFBTU1Ow2Ww4derUyXPnzr19fz7tdjsBQAghTP0ZNJPBwcGvuru7DyqKogGAoijwer2wjY6OQpZldHR0/FIoFFpKpdJDuq5vREoIoQghtGVZ/zrSV1ZWIhzHiV6v9wfDMMxqtQqO42AbGxtDtVpFKpVCa2vrJVmWp1mWba9Wq0HDMBpGfiAQ+F0QhBv5fD7YDCiTyfTzPB/p7Oz8TpZlk+M4MPXVc3eMXAyFQhd5nn/Q4/HsyGaz4Ww26xBFsShJUqK9vf1yMBgs2+32z+Px+GgzoNnZ2cdpmv6yo6PjBQBKQ6SWZaFcLoNhmJlwODxD0zTK5TK2bduGbDaLTCaDUqmELVu2vAyAjsfjzzcDun79+pOiKOrRaPRQ0xuQoigQQqAoCjRNg2maG8+1cVOpVFS/3/8KRVF0PB5/9v4zsywLCwsLz7W3t5do/EexLAuqqkp+v/+lHTt2jP+d39WrV1/9zyB1PxiKIAgHotHoj03vd5st+b9BKIqCoiiaIAjPDgwMNDA6duzYW/8bpAakqqrq9/sPRKPRzwHccTgc8yMjI0/v379//K8BAAT30BUo/WZ0AAAAAElFTkSuQmCC) no-repeat center;   \
         			;cursor:pointer';  \
			   >\
			 </div>";

		 this.intractiveLayer._viewEle.appendChild(pandiv);

		 let div=document.getElementById("mfgrid:quicklocationpan:search");
		 let button= document.getElementById("mfgrid:quicklocationpan:button");
		 let input= document.getElementById("mfgrid:quicklocationpan:input");
		 
		 div.onmousemove=function(e){
		 	//阻止冒泡事件传递到父控件
       		e.stopPropagation();
		 }

		 div.onclick=function(e){
		 	//阻止冒泡事件传递到父控件
       		e.stopPropagation();
		 }

		 button.onclick=function(e){

		 	let locationStr=input.value;
		 	if(!locationStr){
		 		return;
		 	}
		 	 console.log(locationStr)
		 	 //e.preventDefault();

			//阻止冒泡事件传递到父控件
       		e.stopPropagation();


			
		 	 if(locationStr.length>0){
		 	 	let strs=new Array();
		 	 	if(locationStr.indexOf(",")!=-1){
		 	 		strs = locationStr.split(","); //字符分割
		 	 	}else if(locationStr.indexOf("-")!=-1){
		 	 		strs = locationStr.split("-"); //字符分割
		 	 	}else if(locationStr.indexOf("_")!=-1){
		 	 		strs = locationStr.split("_"); //字符分割
		 	 	}

		 	 	if(strs.length==2){
		 	 		let x= parseInt(strs[0]);
		 	 		let y= parseInt(strs[1]);
		 	 		input.value=x+"-"+y;

		 	 		let event=new Event();
			    	 event.senderObj=that;
					 event.type=EventType.INTRACTIVE;

					 event.eventData={
						"action":"location", 							//动作，拖拽
						"coordPosition":[x,y] 	    				//定位点
					};
					EventCenter.postEvent(event);
					return;
		 	 	}
		 	 	
		 	 }

			alert("坐标不符合规范");

		 }
		

	}

	_initStyle(){
		let cssStr="  div.search{   height: 30px;   width: 130px;    }   div.search  input:nth-child(1){   width: 75px;   height: 100%;   font-size: 16px;   text-indent: 5px;   border: none;   float: left;  }  div.search  input:nth-child(2){   width: 50px;   height: 100%;   font-size: 16px;   letter-spacing: 5px;   border: none;  } ";
		loadStyle(cssStr);
	}
  
	

}

export {QuickLocationPan};
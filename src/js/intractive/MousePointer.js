
import {EventCenter,Event} from '../event/EventCenter.js';
import {EventType} from '../event/event.js';
import {sumOffset} from '../util.js'

class MousePointer{
	constructor(intractiveLayer){
		
		this.currentPosition;
		this.intractiveLayer=intractiveLayer;
		this.intractiveLayer._viewEle.style.cursor="default";
		//鼠标样式
		this.lastCursorStyle="default";
		this.cursorStyle="default";
		this.cursorStyleChange=false; //样式是否需要改变。true 需要改变 ，false-不需要改变/已经改变了
		//监听鼠标样式事件
		EventCenter.regist(this,[EventType.POINTCSS]);
	}


	listenerCall(event){

		if(event.type==EventType.POINTCSS){

			let data=event.eventData;
			let action=data.action;


			
			if(action=="cssChange"){
				//console.log("鼠标指针改变事件"+"  原："+this.cursorStyle+"新"+data.cursorStyle)
				this.changeCursorCss(data.cursorStyle)
			}
		}

	}
    
	changeCursorCss(cursorStyle){
		if(cursorStyle&&cursorStyle!=this.cursorStyle){
			this.lastCursorStyle=this.cursorStyle;
			this.cursorStyle=cursorStyle;
			this.cursorStyleChange=true;
			this.intractiveLayer._viewEle.style.cursor=this.cursorStyle;//改变鼠标样式
			
			
		}
	}
    


}

export {MousePointer};
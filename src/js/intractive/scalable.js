
import {EventCenter,Event} from '../event/EventCenter.js';
import {EventType} from '../event/event.js';
import {sumOffset} from '../util.js'

class Scalable{
	constructor(intractiveLayer){
		
		this.currentPosition;
		this.intractiveLayer=intractiveLayer;

		EventCenter.regist(this,[EventType.NONE]);
	}


	onmousewheel(mouseEvent){
		//刷新鼠标位置
		this.refreshCurrentMousePointerPosition(mouseEvent)

        mouseEvent.preventDefault();
        var e = window.mouseEvent || mouseEvent; // old IE support
        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
        //  alert(delta+"->"+ e.wheelDelta+'->'+ e.detail);

       
         let event=new Event();
    	 event.senderObj=this;
		 event.type=EventType.INTRACTIVE;

		 event.eventData={
			"action":"scale", 							//动作，拖拽
			"startPosition":this.currentPosition, 	    //拖拽起始点
			"scaleSizedelta": delta  					//缩放变化大小
		 };
		 EventCenter.postEvent(event);
    }
    
    onmousemove(mouseEvent){
    		this.refreshCurrentMousePointerPosition(mouseEvent)
			

	}

	refreshCurrentMousePointerPosition(mouseEvent){
	    	let x=mouseEvent.offsetX;
    	    let y=mouseEvent.offsetY;

			this.currentPosition=[x,y];//当前鼠标移动到的位置
	}

}

export {Scalable};
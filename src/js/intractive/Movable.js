
import {EventCenter,Event} from '../event/EventCenter.js';
import {EventType} from '../event/event.js';



class Movable{

	constructor(intractiveLayer){
		let mode=intractiveLayer.operationMode;

		this.intractiveLayer=intractiveLayer;


		//鼠标状态 1-按下 0-弹起 
		this._mouseUpStatus=0;
		//鼠标移入状态1-移入  0-移出
		this._mouseInStauts=0;

		//移动状态 0-非移动  1-移动。在鼠标按下时重置为未移动，鼠标移动事件时设为 移动状态
		this._movingStatus=0;
		EventCenter.regist(this,[EventType.NONE]);
	}


   /* 鼠标点击逻辑
	mouseEvent-鼠标事件
   */
    onmousedown(mouseEvent){
    	this._movingStatus=0
    	this._mouseUpStatus=1;
    	

    	

    	let offsetXY=this.intractiveLayer.getViewOffset();
    	//let x=mouseEvent.clientX-offsetXY[0];
    	//let y=mouseEvent.clientY-offsetXY[1];
    	
    	let x=mouseEvent.offsetX;
    	let y=mouseEvent.offsetY;
    	this._mouseLastDragPosition=[x,y];
    	this._mouseDownPosition=[x,y];
    }

	/* 鼠标点击放开逻辑
	mouseEvent-鼠标事件
   */
    onmouseup(mouseEvent){
    	this._mouseUpStatus=0;
    }


    onmouseover(mouseEvent){
    	this._mouseInStauts=1;
    }

    onmouseout(mouseEvent){
    	this._mouseInStauts=0;
    	this._mouseUpStatus=0; //移出视图 ，也当做松开鼠标键
    }

    /*鼠标单击*/
     onclick(mouseEvent){
    	//如果鼠标处于移动事件，则不触发点击事件
    	if(this._movingStatus==1){
    		return
    	}
    	let event=new Event();
    	let that=this;
    	event.senderObj=this;
		event.type=EventType.INTRACTIVE;

    	event.eventData={
				"action":"click", //单击
				"startPosition":that._mouseDownPosition			//点击起始点
			};
		EventCenter.postEvent(event);
    }

	/* 鼠标移动逻辑
	mouseEvent-鼠标事件
	return
   */
    onmousemove(mouseEvent){
    	this._movingStatus=1
    	//console.log("moving")
    	if(this._mouseUpStatus==1&&this._mouseInStauts==1){
	    		//鼠标按下并移动--拖动操作


	    	let event=new Event();
	    	event.senderObj=this;
			event.type=EventType.INTRACTIVE;

			let that=this;

			let x=mouseEvent.offsetX;
    	    let y=mouseEvent.offsetY;

			//let moveDistance=[mouseEvent.clientX-that._mouseLastDragPosition[0],mouseEvent.clientY-that._mouseLastDragPosition[1]]
			let moveDistance=[x-that._mouseLastDragPosition[0],y-that._mouseLastDragPosition[1]]


			if(moveDistance[0]==0&&moveDistance[1]==0){
				//如果未移动
				return;
			}
			
			event.eventData={
				"action":"drag", //动作，拖拽
				"startPosition":that._mouseDownPosition, 				//拖拽起始点
				"movePosition":[x,y], //拖拽到达点
				"lastPosition":that._mouseLastDragPosition , 			//鼠标上次移动的位置
				"moveDistance":moveDistance
			};

	    	EventCenter.postEvent(event);

	    	//更新上一次鼠标拖动位置为当前位置
	    	this._mouseLastDragPosition=[x,y];
    	}else{
    		//鼠标弹起-鼠标移动操作
    		//doNothing
    	}
    }


}

export {Movable};
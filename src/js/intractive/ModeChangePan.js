
import {EventCenter,Event} from '../event/EventCenter.js';
import {EventType} from '../event/event.js';
import {loadFile,loadStyle} from '../util.js';
import {OperationMode} from '../layer/IntractiveLayer.js';


/*模式切换控制面板*/
class ModeChangePan{

	constructor(intractiveLayer){

		

		this.intractiveLayer=intractiveLayer;
		this.mode=intractiveLayer.operationMode;
		
		this._initStyle();
		this._initPan();
		
		//需要对模式进行设置.移动模式
		intractiveLayer.operationMode=OperationMode.MOVEMODE;
	}


	_initPan(){


		let that=this;
		
		//加载面板元素
		let pandiv=document.createElement("div");
		pandiv.style="float:right;height:30px;margin-top:10px;margin-right:10px;";
		

		 pandiv.innerHTML=
		   "<div class='toggle-button-cover' style='border:1px'>\
		      <div class='button-cover'>\
		        <div class='button b2' id='button-16'>\
		          <input type='checkbox' class='checkbox'>\
		          <div class='knobs'></div>\
		          <div class='layer'></div>\
		        </div>\
		      </div>";

		 this.intractiveLayer._viewEle.appendChild(pandiv);

		 let button= document.getElementById("button-16");
		 button.onclick=function(e){
		 	 //e.preventDefault();
		 	 if(OperationMode.MOVEMODE==that.intractiveLayer.operationMode){
		 	 	that.intractiveLayer.operationMode=OperationMode.SELECTMODE;
		 	 }else{
		 	 	that.intractiveLayer.operationMode=OperationMode.MOVEMODE;
		 	 }
		 	 //阻止冒泡，将事件传递到父组件
       		e.stopPropagation();
		 }
		

	}

	_initStyle(){
		let cssStr="*{    user-select: none;    -webkit-tap-highlight-color:transparent;}*:focus{    outline: none;}body{    font-family: Arial, Helvetica, sans-serif;    margin: 0;    background-color: #f1f9f9;}#app-cover{    display: table;    width: 600px;    margin: 80px auto;    counter-reset: button-counter;}.row{    display: table-row;}.toggle-button-cover{    display: table-cell;    position: relative;    width: 75px;    height: 30px;    box-sizing: border-box;}.button-cover{    height: 30px;    margin: 0px;    background-color: #fff;    box-shadow: 0 10px 20px -8px #c5d6d6;    border-radius: 4px;}.button-cover:before{    counter-increment: button-counter;    content: counter(button-counter);    position: absolute;    right: 0;    bottom: 0;    color: #d7e3e3;    font-size: 12px;    line-height: 1;    padding: 5px;}.button-cover, .knobs, .layer{    position: absolute;    top: 0;    right: 0;    bottom: 0;    left: 0;}.button{    position: relative;    top: 50%;    width: 74px;    height: 30px;    margin: -15px auto 0 auto;    overflow: hidden;}.button.r, .button.r .layer{    border-radius: 100px;}.button.b2{    border-radius: 2px;}.checkbox{    position: relative;    width: 100%;    height: 100%;    padding: 0;    margin: 0;    opacity: 0;    cursor: pointer;    z-index: 3;}.knobs{    z-index: 2;}.layer{    width: 100%;    background-color: #ebf7fc;    transition: 0.3s ease all;    z-index: 1;}/* Button 16 */#button-16 .knobs:before{    content: '拖动';    position: absolute;    top: 1px;    left: 4px;    width: 40px;    height: 20px;    color: #fff;    font-size: 10px;    font-weight: bold;    text-align: center;    line-height: 1;    padding: 9px 2px;    background-color: #03A9F4;    border-radius: 2px;    transition: 0.3s ease all, left 0.3s cubic-bezier(0.18, 0.89, 0.35, 1.15);    }#button-16 .checkbox:active + .knobs:before{    width: 46px;}#button-16 .checkbox:checked:active + .knobs:before{    margin-left: -26px;}#button-16 .checkbox:checked + .knobs:before{    content: '选择';    left: 30px;    background-color: #F44336;}#button-16 .checkbox:checked ~ .layer{    background-color: #fcebeb;}";
		loadStyle(cssStr);
	}
  
	

}

export {ModeChangePan};
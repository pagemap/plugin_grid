
import {CoordinateSystem} from '../coordinateSystem/CoordinateSystem.js';
import {CartesianSystem} from '../coordinateSystem/CartesianSystem.js';
import {Draw} from '../draw/Draw.js';
import {CanvasDraw} from '../draw/canvas/CanvasDraw.js';
import {EventCenter,Event} from '../event/EventCenter.js';
import {EventType} from '../event/event.js';
import {GridDataCenter,BlockData,UnitData} from '../data/RenderData.js'
import {CoordPosition,ViewPXPosition} from '../coordinateSystem/position.js'
import {PagemapDataSource} from '../data/PagemapDataSource.js';


/*底图层
主要功能-绘制相关网格 及 底图的文字


*/
class GridLayer{

	constructor(options) {
		let that=this;
		this.canvas=options.canvas;

		let viewSize=[this.canvas.width,this.canvas.height];
		this.coordinateSystem=new CoordinateSystem(new CartesianSystem(),viewSize);
		//设置坐标系的中心点为canva画布的中心坐标。以为在中心所以不用考虑 坐标转换问题

		this.canvasDraw=new CanvasDraw(this.canvas,this.coordinateSystem);
		this.draw=new Draw(this.canvasDraw);

		
		//网格数据中心
		if(options.dataSource){
			this.gridDataCenter=new GridDataCenter(options.dataSource);
		}else{
			this.gridDataCenter=new GridDataCenter(new PagemapDataSource());
		}

		 //初始化z的层级开关
		 this.maxZ=options.maxZ;
		 this.minZ=options.minZ;
		 //this.zIndex=options.zIndex;
		 //this.gridDataCenter.setZIndex(this.zIndex)
       	 this.setZ(options.zIndex)
		

		this.render();

		//在事件中心注册
		EventCenter.regist(this,[EventType.COORDINATECHANGE,EventType.INTRACTIVE],this);

		setTimeout(function(){that.checkMousePinter()},500);
		
		
		
	}

	/*获取当前层级标志
	  return 0-在有效范围内（非最值） 1-最大值 -1 最小值 ,-2 层级开关未打开
	*/
	zIndexFlag(){
		if(this.zIndex==-1){
			return -2;
		}
		if(this.zIndex==this.maxZ){
			return 1;
		}else if(this.zIndex==this.minZ){
			return -1;
		}else{
			return 0;
		}
	}

	setDataSource(dataSource){
		this.gridDataCenter.setDataSource(dataSource);
	}

	render(){
		//按坐标视图中心计算偏移


		let pxPerUnitWidth=this.coordinateSystem.pxPerUnitWidth; //单位长的像素距离
		let pxPerUnitHeight=this.coordinateSystem.pxPerUnitHeight;//单位高的像素距离
		//计算可视区域的线
		let centerpxPosition=this.coordinateSystem.getViewCenter();

		let canvasWidth=this.canvas.width;
		let canvasHeight=this.canvas.height;

		//计算偏移量
		let offset=this.calculateOffsetByCoordinateCenter(centerpxPosition,pxPerUnitWidth,pxPerUnitHeight,canvasWidth,canvasHeight);

		//根据偏移量画图
		this.draw.cleanGrid();
		this.draw.drawGrid(offset[0],offset[1],pxPerUnitWidth,pxPerUnitHeight);

		//根据数据填充网格
		let coordExtend=this.coordinateSystem.getViewCoordExtend(this.canvas);
		let unitDatas=this.gridDataCenter.getExtendData(new CoordPosition(coordExtend[0],coordExtend[1]),new CoordPosition(coordExtend[2],coordExtend[3]))
		//将unitDatas 的坐标换算成viewPxPosition
		this._fillRenderDataByViewPxPosition(unitDatas);
		this.draw.fillGrid(unitDatas); //填充格子颜色

		this.draw.fillText(unitDatas); //填充文字
		


	}

	/*对网格数据进行必要设置
	如设置网格的左上角坐标点
	*/
	_fillRenderDataByViewPxPosition(unitDatas){
		for(let i=0;i<unitDatas.length;i++){
			let item=unitDatas[i];
			let viewPXPositionExtend=this.coordinateSystem.getViewPxPositionExtendFromCoordinate(item.x,item.y,this.canvas);
			
			//找到网格的左上角
			item.pxX=Math.floor(viewPXPositionExtend[0].x);
			item.pxY=Math.floor(viewPXPositionExtend[1].y);
			
			item.pxWidth=Math.floor(viewPXPositionExtend[1].x-viewPXPositionExtend[0].x);
			item.pxHeight=Math.floor(viewPXPositionExtend[1].y-viewPXPositionExtend[0].y);
			item.color=item.data.color;


		}
		return unitDatas;
	}


	listenerCall(eventIn){
		let that=this;
		//console.log("gridLayer收到事件"+eventIn.type)
			/*event.eventData={
				"action":"click", //单击
				"startPosition":that._mouseDownPosition			//拖拽起始点
			};*/
		if(eventIn.type==EventType.INTRACTIVE){
			if(!eventIn.eventData){
				return;
			}
			if(!eventIn.eventData.action||eventIn.eventData.action!="click"){
				return;
			}
			let viewPosition={x:eventIn.eventData.startPosition[0],y:eventIn.eventData.startPosition[1]};
			//从视图坐标得到 坐标轴坐标
			let coord=this.coordinateSystem.getCoordFromViewPxPosition(viewPosition);

			//发送控件输出事件
			//发送输出事件
			let event=new Event();
			event.senderObj=this;
			event.type=EventType.OUTPUT;
			event.eventData={
				"action":"clickGrid",   //动作，选择
				"zIndex":that.zIndex, 	//当前层级			
				"coord":coord,          //指针坐标
			};

	    	EventCenter.postEvent(event);
				

		}

	}

	/*根据可视区域中西的在坐标系上的坐标点，计算网格视图的偏移量
	pxPosition-视图中心在坐标系中的像素定位
	pxPerUnitWidth- 单位宽的像素距离（坐标系中）
	pxPerUnitHeight- 单位高的像素距离（坐标系中）
	canvasWidth- 画布像素宽
	canvasHeight-画布像素高
	*/
	calculateOffsetByCoordinateCenter(pxPosition,pxPerUnitWidth,pxPerUnitHeight,canvasWidth,canvasHeight){
		let startX=pxPosition.x -(canvasWidth/2);
		let startY=pxPosition.y-(canvasHeight/2);

		let offsetX=0;
		let offsetY=0;
		if(startX>0){
			 offsetX=pxPerUnitWidth-startX%pxPerUnitWidth;
		}else{
			 offsetX=0-startX%pxPerUnitWidth
		}

		if(startY>0){
			 offsetY=pxPerUnitHeight-startY%pxPerUnitHeight;
		}else{
			 offsetY=0-startY%pxPerUnitHeight;
		}
		
		
		if(offsetX==pxPerUnitWidth){
			offsetX=0;
		}
		if(offsetY==pxPerUnitHeight){
			offsetY=0;
		}
		return [offsetX,offsetY]
	}

	/*视图移动
	viewMoveDistance-[xdistance,ydistance]
	
	*/
	moveView(viewMoveDistance){
		if(viewMoveDistance[0]==0&&viewMoveDistance[1]==0){
			return;
		}
		let that=this;
		let canvaseDistance=viewMoveDistance;
	 	let viewDistance=that.coordinateSystem.translateDistanceFromCanvasXY(canvaseDistance);
	 	that.coordinateSystem.moveViewCenter(viewDistance[0],viewDistance[1]);
		that.render();

	}

	/*视图选中
	*/
	selectView(startCanvasViewPxPosition,endCanvasViewPxPosition){
		

		//操作驱动数据改变(待定，最优方法应当如此)
		let that=this;
		let minX=startCanvasViewPxPosition.x;
		let maxX=endCanvasViewPxPosition.x;
		let minY=startCanvasViewPxPosition.y;
		let maxY=endCanvasViewPxPosition.y;
		if(startCanvasViewPxPosition.x>endCanvasViewPxPosition.x){
			minX=endCanvasViewPxPosition.x;
			maxX=startCanvasViewPxPosition.x;
		}

		if(startCanvasViewPxPosition.y>endCanvasViewPxPosition.y){
			minY=endCanvasViewPxPosition.y;
			maxY=startCanvasViewPxPosition.y;
		}

		
		
		//寻找画图的起点-左上角
		let leftTop=new ViewPXPosition(minX,minY);
		let leftTopViewPxPosition=that.coordinateSystem.translateFromCanvasXY(leftTop,this.canvas);

		let width=maxX-minX;
		let height=maxY-minY;
		//画出选择区域
		this.render();
		this.draw.drawSelected(leftTopViewPxPosition,width,height); //画出选择区域


		//画出选择区域的 左下角和右上角坐标
		let leftBottom=new ViewPXPosition(minX,maxY);
		let rightTop=new  ViewPXPosition(maxX,minY);
		
		let leftBottomViewPxPosition=that.coordinateSystem.translateFromCanvasXY(leftBottom,this.canvas);
		let rightTopViewPxPosition=that.coordinateSystem.translateFromCanvasXY(rightTop,this.canvas);

		//计算像素区域的坐标
		let start=that.coordinateSystem.translateFromCanvasXY(startCanvasViewPxPosition,this.canvas);
		let end=that.coordinateSystem.translateFromCanvasXY(endCanvasViewPxPosition,this.canvas);
		let coordExtend=that.coordinateSystem.getCoordinateExtendByViewExtend(start,end)

		let txt1=coordExtend[2]+","+coordExtend[3]; //右上角数字
		let txt2=coordExtend[0]+","+coordExtend[1]; //左下角数字
		
		this.draw.drawSelectText(leftBottomViewPxPosition,rightTopViewPxPosition,txt2,txt1,this.canvas.width,this.canvas.height);

		//发送输出事件
		let event=new Event();
		event.senderObj=this;
		event.type=EventType.OUTPUT;
		event.eventData={
				"action":"select", //动作，选择
				"extent":coordExtend, 				//坐标范围[x1,y1,x2,y2]
			};

	    	EventCenter.postEvent(event);
	}

	selectGrid(startCanvasViewPxPosition){
		let start=this.coordinateSystem.translateFromCanvasXY(startCanvasViewPxPosition,this.canvas);
		let coordPosition=this.coordinateSystem.getCoordinateFromViewPxPosition(start);

		let txt=coordPosition.x+","+coordPosition.y; //左下角数字
		this.render();
		this.draw.drawGridText(start,txt,this.canvas.width,this.canvas.height);
	}


	/*以鼠标中心点为锚点缩放视图
		return 0- 缩放有效范围  1-超出最大缩放比例（像素最大） -1 超出最小缩放比例（像素最小）
	*/
	scaleView(startCanvasViewPxPosition,scaleSizedelta){


		if(!startCanvasViewPxPosition){
			//已左下角为锚点缩放
			startCanvasViewPxPosition=new ViewPXPosition(0,this.canvas.height);
		}
		let start=this.coordinateSystem.translateFromCanvasXY(startCanvasViewPxPosition,this.canvas);
		let flag=this.coordinateSystem.scaleSystem(start,scaleSizedelta);
		this.render();
		return flag;

		//检查样式改变
		let that=this
		setTimeout(function(){that.checkMousePinter()},500);
	}


	/*

	  对层级进行变化
	  如果层级变化有效，返回0
	  超出最大maxZ，返回1，超出最小minZ，返回-1
	*/
	scaleZ(pxStartCoord,scaleSizedelta){
		
		
		let z=this.zIndex+scaleSizedelta;
		if(z<=this.maxZ&&z>=this.minZ){
			//有效范围
			this.setZ(z);

			//层级转换后需要重新设置视图中心点，否则缩放效果 如同以左下角会锚点缩放
			//层级缩放始终已视图中心为缩放锚点
			this.coordinateSystem.scaleLevel(pxStartCoord,scaleSizedelta);
			this.render();

			//检查样式改变
			let that=this;
			setTimeout(function(){that.checkMousePinter()},500);
			return 0;
		}else{
			if(z>this.maxZ){
				return 1;
			}else{
				return -1;
			}
		}
	}


	/*设置Z 层级
	设置后需要调用render 才会看到该效果，如果不调用，则试图不会刷新
	*/
	setZ(zIndex){
		this.zIndex=zIndex;
		this.gridDataCenter.setZIndex(this.zIndex)
	}

	/*设置中心坐标*/
	locationView(coordPosition){
		this.coordinateSystem.setCoordCenter(coordPosition.x,coordPosition.y);
		this.render();
	}

	/*设置中心像素坐标*/
	locationViewPxPosition(coordPxPosition){
		this.coordinateSystem.setViewCenter(coordPxPosition.x,coordPxPosition.y);
		this.render();
	}

	//检查鼠标是否需要变换样式
	checkMousePinter(){
		//console.log("检查鼠标样式")

		//发送鼠表css事件
		let event=new Event();
		event.senderObj=this;
		event.type=EventType.POINTCSS;

		//鼠标指针在最大层级时需要变化为手样式
		if(this.zIndex==this.maxZ){
			event.eventData={
					"action":"cssChange",    //样式改变
					"cursorStyle":"pointer", //手指模式
				};
		}else{
			event.eventData={
					"action":"cssChange",    //样式改变
					"cursorStyle":"default", //默认指针模式
				};
		}
		EventCenter.postEvent(event);
	}


}

export {GridLayer};
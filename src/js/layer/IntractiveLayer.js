
import {EventType} from  '../event/event.js';
import {EventCenter,Event} from '../event/EventCenter.js';
import {Movable} from '../intractive/Movable.js'
import {Scalable} from '../intractive/Scalable.js'
import {Selectable} from '../intractive/Selectable.js'
import {ModeChangePan} from '../intractive/ModeChangePan.js';
import {QuickLocationPan} from '../intractive/QuickLocationPan.js';
import {MousePointer} from '../intractive/MousePointer.js';


import {sumOffset} from '../util.js'
class IntractiveLayer{

	/*viewEle-用户交互操作的元素ele
	*/
	constructor(options) {

		let that=this;
		this._viewEle=options.viewEle;


		//operationMode-操作模式
		this.operationMode=OperationMode.MOVEMODE;

		//定义交互能力
		this.abilities=[]; //功能数组
		this.abilitiesMap={};//功能map { 操作模式：[功能对象1，功能对象2]}
		
		this.registeAbility(new Movable(that),OperationMode.MOVEMODE)  //拖拽移动功能
		this.registeAbility(new Selectable(that),OperationMode.SELECTMODE) //选择功能
		this.registeAbility(new Scalable(that),"ALL") //缩放工呢

		this.registeAbility(new ModeChangePan(that),"ALL")  	//模式控制面板
		this.registeAbility(new QuickLocationPan(that),"ALL")  //快速定位面板
		this.registeAbility(new MousePointer(that),"ALL")  		//鼠标相关功能
		


		// this._viewEle.style.pointerEvents="none";


		this._viewEle.onmousedown=function(e){
			//console.log("IntractiveView onMouseDown")
			that.onmousedown=e;
			that.callAbility("onmousedown",e);

			//that.mouseDown(e);
		}

		this._viewEle.onmouseup=function(e){
			//console.log("IntractiveView onMouseUp")
			//that.mouseUp(e);
			//that.onmouseup=e;
			that.callAbility("onmouseup",e);
		}

		this._viewEle.onmousemove=function(e){
			that.onmousemove=e;
			//console.log("IntractiveView onMousemove")

			//that.mouseMove(e);
			//对单击操作中的mouseMove 过滤
			if(that.onmousedown){
				//console.log(that.onmousedown)
				//console.log(that.onmousemove)
				if(that.onmousedown.x==that.onmousemove.x&&that.onmousedown.y==that.onmousemove.y){
					return;
				}
				
			}
			
			that.callAbility("onmousemove",e);
		}

		this._viewEle.onmouseover=function(e){
			//console.log("IntractiveView onmouseover")
			//that.mouseOver(e);
			that.callAbility("onmouseover",e);
		}

		this._viewEle.onmouseout=function(e){
			//console.log("IntractiveView onmouseout")
			//that.mouseOut(e);
			that.callAbility("onmouseout",e);
		}

		this._viewEle.onclick=function(e){
			//console.log("IntractiveView onmouseout")
			//that.mouseOut(e);
			that.callAbility("onclick",e);
		}

		this._viewEle.onmousewheel=function(e){
			//console.log("IntractiveView onmouseout")
			//that.mouseOut(e);
			that.callAbility("onmousewheel",e);
		}


		EventCenter.regist(this,[EventType.NONE],this.eventCall);

    }


    /*注册能力对象，并将对象能力绑定到模式上
	abilitie-功能对象，仅限于intractive包下的对象
	operationMode-操作模式， 如果为“ALL” 表示所有模式都试用
    */
    registeAbility(abilitie,operationMode){

    	this.abilities.push(abilitie);
    	
    	if(operationMode in this.abilitiesMap){
    		this.abilitiesMap[operationMode].push(abilitie);
    	}else{
    		this.abilitiesMap[operationMode]=[abilitie];
    	}
    	
    }


    /*根据模式获取能力对象
	mode-OperationMode
	return-[abilities,abilities] 功能数组
    */
    getAbilitesByOperationMode(mode){
    	if(! (mode in OperationMode)){
    		throw "非法的操作模式"
    	}
    	
    	//获取指定模式下的功能
    	let abilities=[];
    	if(mode in this.abilitiesMap){
			abilities=abilities.concat(this.abilitiesMap[mode]);	
    	}

    	//获取all模式下的功能
    	abilities=abilities.concat(this.abilitiesMap["ALL"]);	

    	return abilities;
    }

    /*调用能力
	functionName-功能的方法名称
    */
    callAbility(functionName,event){
    	//获取当前操作模式下的功能列表
    	let abilities=this.getAbilitesByOperationMode(this.operationMode);
    	
    	for(var i=0;i<abilities.length;i++){

    		//执行方法
			let abilitie=abilities[i];
			let realfunction=abilitie[functionName];
			if(!realfunction){
				//方法不存在就不会调用
			}else{
				realfunction.call(abilitie,event)

			}

    	}
    }

    /*事件处理方法
	event-事件类
    */
    eventCall(event){

    	console.log("收到事件");
    	console.log(event);
    }

   	setOperationMode(mode){
   		if(! (mode in OperationMode)){
    		throw "非法的操作模式"
    	}
    	this.operationMode=mode;
   	}

   /*	获取视图在浏览器中的偏移量
	用于计算鼠标在layer中的位置
	return-[offsetX,offsetY] x y的偏移量
   */
   	getViewOffset(){
   		


   		return sumOffset(this._viewEle);
   	}

}

let OperationMode={
	"MOVEMODE":"MOVEMODE",  //移动模式
	"SELECTMODE":"SELECTMODE" //选择模式
}

export {IntractiveLayer,OperationMode};

import {ViewPXPosition} from '../../coordinateSystem/position.js';

/*主要对canvas 画图进行封装
所有坐标参数均使用笛卡尔坐标系 dcs

*/
class CanvasDraw{

   /*构造方法
    canvas-canvas对象
    coorinateSystem-坐标系对象
   */
   constructor(canvas,coorinateSystem) {
		  this.canvas=canvas;
      this.coorinateSystem=coorinateSystem;
      this.ctx = canvas.getContext("2d");
    }


   /* 画填充矩形
    */
   fillRect(x,y,width,height){
    let  canvasXY= this.translateToCanvasXY([x,y] )
     if(this.fillStyle){
       this.ctx.fillStyle=this.fillStyle;
     }
      this.ctx.fillRect(canvasXY[0],canvasXY[1],width,height);
   }

  

   beginPath(){
      this.ctx.beginPath();
   }

   closePath(){
   	 this.ctx.closePath();
   }

   

   stroke(){
    if(this.strokeStyle)
    this.ctx.strokeStyle=this.strokeStyle
   	 this.ctx.stroke();
   }

   lineTo(x,y){
     let canvasXY= this.translateToCanvasXY([x,y] )
   	 this.ctx.lineTo(canvasXY[0],canvasXY[1]);
    // console.log("lineTo-["+canvasXY[0]+","+canvasXY[1]+"]"  )
   }

   moveTo(x,y){
    let canvasXY= this.translateToCanvasXY([x,y] );
   	 this.ctx.moveTo(canvasXY[0],canvasXY[1]);
    // console.log("moveTo-["+canvasXY[0]+","+canvasXY[1]+"]" )
   }

   clearRect(x,y,width,height){
     let canvasXY= this.translateToCanvasXY([x,y] );
     this.ctx.clearRect(canvasXY[0],canvasXY[1]-height,width,height);
   }
  

  /* 对相应的坐标系（原点不同方向）像素坐标进行转换成canva像素坐标
  xy-[x,y] 在坐标系中的视图像素坐标
  return-[x,y] 在canva中的视图像素坐标
  */
   translateToCanvasXY(xy){
     let originPosition=new ViewPXPosition(xy[0],xy[1]);
     let viewPxPosition= this.coorinateSystem.translateToCanvasXY(originPosition,this.canvas);
     return [viewPxPosition.x,viewPxPosition.y]

   }

   
   fillText( text, x, y){
        let canvasXY= this.translateToCanvasXY([x,y] );

        var ctx = this.ctx;
        if(this.fillStyle)
          ctx.fillStyle =this.fillStyle;  //设置填充颜色为紫色
       
        if(this.fillStyle)             
          ctx.font =this.font;           //设置字体\
       
        if(this.fillStyle)
          ctx.textBaseline =this.textBaseline;            //设置字体底线对齐绘制基线
       
        if(this.fillStyle)
          ctx.textAlign =this.textAlign;                 //设置字体对齐的方式
       
       ctx.fillText( text, canvasXY[0], canvasXY[1] );        //填充文字

       
    }

    strokeText( text, x, y){
        let canvasXY= this.translateToCanvasXY([x,y] );

        var ctx = this.ctx;
        if(this.fillStyle)
          ctx.fillStyle =this.fillStyle;  //设置填充颜色为紫色
       
        if(this.fillStyle)             
          ctx.font =this.font;           //设置字体\
       
        if(this.fillStyle)
          ctx.textBaseline =this.textBaseline;            //设置字体底线对齐绘制基线
       
        if(this.fillStyle)
          ctx.textAlign =this.textAlign;                 //设置字体对齐的方式
       
       ctx.strokeText( text, canvasXY[0], canvasXY[1] );        //填充文字

       
    }

     /*设置字体
     font-字体，包含大小及格式，如 40pt Calibri
     */
    setFont(font){
        ctx.font(font);
    }

}

export {CanvasDraw };
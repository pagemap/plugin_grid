

class Draw{

	constructor(canvasDraw){
		this.canvasDraw=canvasDraw;
		this.gridLineColor="#B6C29A"
	}

    /*画网格
    offsetX-第一条纵线偏移量
    offsetY-第一条横线偏移量
    pxPerUnitWidth-单位宽像素距离
    pxPerUnitHeight-单位高像素距离
    */
	drawGrid(offsetX,offsetY,pxPerUnitWidth,pxPerUnitHeight){
        //去掉小数点-用于控制线条模糊度问题
          offsetX=Math.floor(offsetX);
          offsetY=Math.floor(offsetY);
		if(offsetX>=pxPerUnitWidth||offsetY>=pxPerUnitHeight){
			console.log(offsetX,offsetY,pxPerUnitWidth,pxPerUnitHeight);
			throw "偏移量应小于单位距离"
		}
			var ctx = this.canvasDraw;
			ctx.strokeStyle = this.gridLineColor;
            ctx.lineWidth = 1;

            //偏移0.5的量是因为canvas中渲染线条更清晰
            //画横线
            let sysOffsetX=0.5;
            let sysOffsetY=0.5;
            offsetY=offsetY+sysOffsetY;
            offsetX=offsetX+sysOffsetX;
            for(var i =offsetY;i<ctx.canvas.clientHeight;i+=pxPerUnitHeight){
                ctx.beginPath();
                ctx.moveTo(0,i);
                ctx.lineTo(ctx.canvas.clientWidth,i);
                ctx.stroke();
            }

            //画竖线
            for(var i = offsetX;i<ctx.canvas.clientWidth;i+=pxPerUnitWidth){
                ctx.beginPath();
                ctx.moveTo(i,0);
                ctx.lineTo(i,ctx.canvas.clientHeight);
                ctx.stroke();
            }
          	

             ctx.closePath();
	}


	cleanGrid(){
         var ctx = this.canvasDraw;
         ctx.clearRect(0,0,ctx.canvas.width,ctx.canvas.height);
    }

    /*画出选择区域(对外应提供 按坐标系的逻辑规则参数，而不是canva的逻辑规则参数)
    */
    drawSelected(leftTopViewPxPosition,width,height){
         var ctx = this.canvasDraw;
         //画出矩形  
         let color='rgba(0, 255, 127, 0.5)';
         ctx.fillStyle = color;
         ctx.fillRect(leftTopViewPxPosition.x,leftTopViewPxPosition.y,width,height);
         ctx.closePath();

    }

    /*画出选择区域的文本(对外应提供 按坐标系的逻辑规则参数，而不是canva的逻辑规则参数，待修改)
    leftBottomViewPxPosition-左下角的点
    rightTopViewPxPosition-右上角的点
    leftBottomText-左下角文字
    rightTopText-右上角文字
    canvasWidth-画布宽度
    canvasHeight-画布高度
    */
    drawSelectText(leftBottomViewPxPosition,rightTopViewPxPosition,leftBottomText,rightTopText,canvasWidth,canvasHeight){
        let fillStyle = "red";               //设置填充颜色为紫色
        let font = '20px "微软雅黑"';           //设置字体
        //画出范围坐标右上角
        let txt1=rightTopText;

        let textAlign1="left";
        let textBaseline1="bottom";
        if((canvasWidth-rightTopViewPxPosition.x)<45)
            textAlign1="right";

        if((canvasHeight-rightTopViewPxPosition.y)<20)
            textBaseline1="top";
        
        this.drawText(rightTopViewPxPosition.x,rightTopViewPxPosition.y,txt1,textAlign1,textBaseline1,fillStyle,font);
        
         //画出范围坐标左下角 

         if(leftBottomText==rightTopText){
            //如果两个点的坐标一致，则只画一个坐标点文字就可以了
            return;
         }

        let txt2=leftBottomText;

        let textAlign2="right";
        let textBaseline2="top";;
        if(leftBottomViewPxPosition.x<45)
            textAlign2="left";

        if(leftBottomViewPxPosition.y<20)
            textBaseline2="bottom";
        this.drawText(leftBottomViewPxPosition.x,leftBottomViewPxPosition.y,txt2,textAlign2,textBaseline2,fillStyle,font); 


    }


    drawGridText(viewPxPosition,text,canvasWidth,canvasHeight){
        let fillStyle = "red";               //设置填充颜色为紫色
        let font = '20px "微软雅黑"';           //设置字体
        //画出范围坐标右上角
        let txt1=text;

        let textAlign1="left";
        let textBaseline1="bottom";;
        if((canvasWidth-viewPxPosition.x)<45)
            textAlign1="right";

        if((canvasHeight-viewPxPosition.y)<20)
            textBaseline1="top";
        
        this.drawText(viewPxPosition.x,viewPxPosition.y,txt1,textAlign1,textBaseline1,fillStyle,font);
        
    }


    drawText(viewPxX,viewPxY,text,textAlign,textBaseline,fillStyle,font){
        var ctx = this.canvasDraw;
        ctx.fillStyle = fillStyle;               //设置填充颜色为紫色
        ctx.font =font;           //设置字体
        ctx.textAlign=textAlign;
        ctx.textBaseline=textBaseline;
        //ctx.strokeText( "left", 450, 400 );
        ctx.fillText( text, viewPxX, viewPxY );        //填充文字
        ctx.closePath();
    }



    /*填充单个网格 (对外应提供 按坐标系的逻辑规则参数，而不是canva的逻辑规则参数，待修改)
    unitDataArray-填充数据数组
    */
    fillGrid(unitDataArray){
        var ctx = this.canvasDraw;
        //循环填充区域
        for(let i=0;i<unitDataArray.length;i++){
             

             let item=unitDataArray[i];
             
             ctx.fillStyle = item.color;
             //+1  -1的原因，因为因为保证不遮盖掉网格线。
             //为什呢y是0.5.中间可能有计算偏移了 部分像素点-以后可能需要改
             ctx.fillRect(item.pxX+1,item.pxY+0,item.pxWidth-1,item.pxHeight-1 );
        }
        ctx.closePath();
    }

   /* 对视图进行文字填充。标注

   */
    fillText(unitDataArray){
        var ctx = this.canvasDraw;
        //循环填充区域
        for(let i=0;i<unitDataArray.length;i++){
             
            let item=unitDataArray[i];
           
             if(!item.data||!item.data.text){
                continue;
             }
             let text=item.data.text;
             let fillStyle = text.color;
             let font=text.size+"pt Calibri";
             this.drawText(item.pxX,item.pxY,text.content,"center","middle",fillStyle,font)

        }
    }
}


class FillData{
    
    constructor(viewPxX,viewPxY,width,height,color){
        this.viewPxX=viewPxX;
        this.viewPxY=viewPxY;
        this.width=width;
        this.height=height;
        this.color=color;
    }
}
export {Draw,FillData} ;
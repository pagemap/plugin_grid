
import {IntractiveLayer,OperationMode} from "./layer/IntractiveLayer.js";
import {GridLayer} from "./layer/GridLayer.js";

import {EventCenter} from "./event/EventCenter.js";
import {EventType} from "./event/event.js";
import {ViewPXPosition,CoordPosition} from './coordinateSystem/position.js';


class MFGrid{ 
	

	constructor(options) {
		

		let viewContainer=options.viewContainer;
		this._viewContainer=viewContainer;
		EventCenter.preInit(new EventCenter());
		//注册事件中心
	  	EventCenter.regist(this,[EventType.ALL])

        //初始网格层

        this._gridView = document.createElement('canvas');
	    this._gridView.style.position = 'absolute';
	    this._gridView.style.zIndex = '100';
	    this._gridView.style.width = '100%';
	    this._gridView.style.height = '100%';
	    this._viewContainer.appendChild(this._gridView);
	    //必须设置canvas的width 和height，css仅设置宽高会引起画面模糊
	     this._gridView.width=viewContainer.clientWidth;
	     this._gridView.height= viewContainer.clientHeight;

		let options1={};
	    options1.canvas=this._gridView;
	    

	    //设置数据源
	    if(options.dataSource){
	    	  options1.dataSource=options.dataSource;
	    	  options1.minZ= options.minZ;
	    	  options1.maxZ=options.maxZ;
	    	  options1.zIndex= options.zIndex;
	    }
	    this.gridLayer=new GridLayer(options1);
	    console.log("gridLayer 初始化完成")
	  	this.gridLayer.render();//初始化后调用一次渲染，防止数据源延迟初始化


	    //初始化交互视图层
	    this._intractiveView = document.createElement('div');
	    this._intractiveView.style.position = 'absolute';
	    this._intractiveView.style.zIndex = '200';
	    this._intractiveView.style.width = '100%';
	    this._intractiveView.style.height = '100%';
	     this._intractiveView.style.opacity=1;
	    this._viewContainer.appendChild(this._intractiveView);
	    let options2={};
	    
	    options2.viewEle=this._intractiveView;
	     //options2.viewEle=this._gridView
	 	this.intractiveLayer=new IntractiveLayer( options2);



    }

	

	/*设置操作模式

	*/
	setOperationMode(mode){
		this.intractiveLayer.setOperationMode(mode);
	}


	/*按事件中心EventCenter规范实现该方法*/
	listenerCall(event){
		let that=this;
		switch(event.type) {

		     case EventType.INTRACTIVE:  //交互事件
		         that.handerIntractive(event);
				 break;
			case EventType.OUTPUT:  	//组件输出事件
		    	 that.handleOutput(event);
		    	 break;
		    case EventType.DATAREFRESH:  	//数据源更新事件
		    	 that.handleDatasourceRefresh(event);
		    	 break;
		     default:
		       // 默认代码块
		} 

	}


	handerIntractive(event){
		let operateMode=this.intractiveLayer.operationMode;
		let that=this;
		switch(event.eventData.action){

			case "drag": //拖拽事件
	 			if(operateMode==OperationMode.MOVEMODE){ //拖拽移动
	 				
	 				that.gridLayer.moveView(event.eventData.moveDistance);

	 			}else if(operateMode==OperationMode.SELECTMODE){  //拖住选择

	 				let startViewPxPosition=new ViewPXPosition(event.eventData.startPosition[0],event.eventData.startPosition[1]);
	 				let endViewPxPosition=new ViewPXPosition(event.eventData.movePosition[0],event.eventData.movePosition[1]);

	 				that.gridLayer.selectView(startViewPxPosition,endViewPxPosition);
	 			}
	 		

	 		break;

	 		case "click"://单击事件
	 			if(operateMode==OperationMode.SELECTMODE){  //单击选择

	 				let startViewPxPosition=new ViewPXPosition(event.eventData.startPosition[0],event.eventData.startPosition[1]);

	 				that.gridLayer.selectGrid(startViewPxPosition);
	 			}
	 		break;


	 		case "scale"://缩放事件

	 				/*缩放逻辑
	 				层级开关开放
						1-先缩放层级
							层级缩放到最大（展示数据少）时，开始缩放视图（慢慢变大）	 展示更详细的数据

						2- 视图缩放到最小时（展示数据最多），开时缩放层级（数据展示变多）  展示更多数据
					
					层级开关不开放
						直接缩放视图
					*/
	 				let scaleSizedelta=event.eventData.scaleSizedelta;
	 				if(!event.eventData.startPosition){
	 					console.log("防止出错")
	 					console.log(event)
	 				}
	 				
	 				let startViewPxPosition=new ViewPXPosition(event.eventData.startPosition[0],event.eventData.startPosition[1]);



					let zIndexFlag=that.gridLayer.zIndexFlag();
					//------------------层级开关关闭
					if(zIndexFlag==-2){
						that.gridLayer.scaleView(startViewPxPosition,scaleSizedelta);
					}
					//------------------层级开关打开
					//放大操作
					if(scaleSizedelta>0&&zIndexFlag!=-2){
						if(zIndexFlag==1){ //最大层级，开始放大视图
							let scaleViewFlag=that.gridLayer.scaleView(startViewPxPosition,scaleSizedelta);
							//console.log("放大视图")
						}else{
							let scaleZFlag=that.gridLayer.scaleZ(startViewPxPosition,scaleSizedelta); //继续放大层级
							//console.log("放大层级")
						}
					}

					//缩小操作
					if(scaleSizedelta<0&&zIndexFlag!=-2){
						
						if(zIndexFlag==1){
							//尝试先缩小视图
							let scaleViewFlag=that.gridLayer.scaleView(startViewPxPosition,scaleSizedelta);
								//console.log("缩小视图")
							if(scaleViewFlag==-1){ //视图缩小到极限，开始缩小层级
								that.gridLayer.scaleZ(startViewPxPosition,scaleSizedelta); 
								//console.log("缩小视图到极限-开始缩小层级")
							}
						}else if(zIndexFlag==-1){ //最小层级，无法继续操作
							
						}else{
							let scaleZFlag=that.gridLayer.scaleZ(startViewPxPosition,scaleSizedelta); //继续缩小层级
							//console.log("缩小层级")
						}
					}



	 		break;


	 		case "location"://定位事件
	 				let coordPosition=new CoordPosition(event.eventData.coordPosition[0],event.eventData.coordPosition[1]);
	 				//let scaleSizedelta=event.eventData.scaleSizedelta;
	 				that.gridLayer.locationView(coordPosition);
	 		break;
			}


	}

	
	/*处理数据输出事件
	*/
	handleOutput(event){
		if(this.handlerFunction){
			this.handlerFunction(event);
		}
	}

	

	/*处理数据源更新事件
	*/
	handleDatasourceRefresh(event){
		 console.log("收到数据源更新通知")
		if(this.gridLayer)
		this.gridLayer.render();
	}


    //------------------------外部API----------------------
   /* 在使用api操作数据时可能感知不道数据变化，可以调用该方法刷新视图
   //重置试图层级为最大层级
   */
    refreshView(){
    	this.gridLayer.render();
    }



	/*设置数据输出处理器
	*/
	setOutputHandler(handlerFunction){
		this.handlerFunction=handlerFunction;
	}


	/*移动位置
	* x-x坐标
	  y-y坐标
	*/
	moveCoordPosition(x,y){
		this.gridLayer.locationView({x:x,y:y})
	}

	/*根据坐标获取数据源中的值
	coordX-x坐标
	coordY-y坐标
	*/
	getGridDataByCoord(coordX,coordY){
		return this.gridLayer.gridDataCenter.getUnitData(coordX,coordY);
	}

	/*设置数据源中的值
	*/
	setUnitData(unitData){
		 this.gridLayer.gridDataCenter.setUnitData(data);
	}


	//-------------------
	/*打开层级开关（该开关控制数据源的获取方式）
		minZ-最小层级
		maxZ-最大层级
	*/
	openZ(z,minZ,maxZ){

	}

	closeZ(){

	}

}

export {MFGrid};
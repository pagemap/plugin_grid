
import {BaseSystem,ORIGIN_MODE} from './BaseSystem.js';
import {ViewPXPosition,PXPosition,CoordPosition} from './position.js';



class CoordinateSystem{

	/*构造方法
	baseSystem-基础坐标系
	viewSize-[widthPx,heightPx]可视区像素范围大小
	*/
	constructor(baseSystem,viewSize) {

		this.coordinateSystem=baseSystem;
		
		//当前可视坐标系范围的中心点
		//this.originCenterPX=new PXPosition(-1,-1); // 初始视图坐标系位置
		this.centerPx=new PXPosition(0,0); //当前的视图中心坐标系像素位置

		//原始为2
		this.scaleSize=2;
		this.pxPerUnitWidth=SCALESIZE[this.scaleSize].pxPerUnitWidth; //单位长的像素距离
		this.pxPerUnitHeight=SCALESIZE[this.scaleSize].pxPerUnitHeight;//单位高的像素距离

		this.viewSize=viewSize;
		
		//初始化视图中心为，view的中心点
		this.setViewCenter(viewSize[0]/2,viewSize[1]/2);

	 }

	 /*对坐标系层级缩放（区别于scaleSystem，scaleSystem是对视图网格放大）
	   层级关系为2倍 缩小或者放大(该条件为必须)
	   缩放效果-锚点中心缩放
	 */
	 scaleLevel(startViewPXPosition,scaleDelta){

	 	/*20210108- 无解。。。当前缩放效果为视图中心缩放

	 	*/
	 	//计算锚点得到坐标
		let anchorPxPosition=this.getPxPositionFromViewPxPosition(startViewPXPosition);
		//重新计算中心坐标轴像素坐标
		//计算 centerPx 的点 到 startViewPXPosition的坐标偏移量
		let xOffsetPx=startViewPXPosition.x -this.viewSize[0]/2;
		let yOffsetPx=startViewPXPosition.y - this.viewSize[1]/2;
		if(scaleDelta>0){//放大
			anchorPxPosition.x= anchorPxPosition.x*2
			anchorPxPosition.y= anchorPxPosition.y*2
		}else if(scaleDelta<0){  //缩小
			anchorPxPosition.x= anchorPxPosition.x/2
			anchorPxPosition.y= anchorPxPosition.y/2
		}

		//计算 centerPx 的像素坐标
		this.centerPx.x=Math.floor(anchorPxPosition.x-xOffsetPx);
		this.centerPx.y=Math.floor(anchorPxPosition.y-yOffsetPx);
	 }


	 /*缩放坐标系
	 startViewPXPosition-锚点viewPXPosition
	 scaleDelta-缩放变化量
	 return 超出最大范围 1，超出最小范围-1 ，有效值0
	 */
	scaleSystem(startViewPXPosition,scaleDelta){

		//计算锚点得到坐标
		let anchorCoordPosition=this.getCoordinateFromViewPxPositionExact(startViewPXPosition);



		//设置缩放级别及相关参数
		let scaleTemp=this.scaleSize+scaleDelta;
		let flag=this.setScaleSize(scaleTemp);

		if(0==flag){//缩放数据有效
			
			//设置缩放级别后，原originCenterPX不会变化，不用计算，centerPx 会产生变化
			//缩放后锚点的坐标及viewPXPosition都不会变化

			//计算 centerPx 的点 到 startViewPXPosition的坐标偏移量
			let xOffsetCoord=(this.viewSize[0]/2-startViewPXPosition.x)/this.pxPerUnitWidth;
			let yOffsetCoord=(this.viewSize[1]/2-startViewPXPosition.y)/this.pxPerUnitHeight;

			let viewCenterCoord=new CoordPosition(anchorCoordPosition.x+xOffsetCoord,anchorCoordPosition.y+yOffsetCoord);
			//计算 centerPx 的像素坐标
			this.centerPx.x=Math.floor(viewCenterCoord.x*this.pxPerUnitWidth) ;
			this.centerPx.y=Math.floor(viewCenterCoord.y*this.pxPerUnitHeight);
		}

		
		return flag;
	}


	/*设置坐标系的缩放级别  SCALSIZE_LIMIT.max SCALSIZE_LIMIT.min 缩放级别最大和最小值范围
	scaleSize-目标缩放大小
	return- 在正常范围 0 ，超出最大范围 1，超出最小范围 -1
	
	*/
	setScaleSize(scaleSize){
		

		if(scaleSize>SCALSIZE_LIMIT.max){
			//超出范围不做响应
			return 1;
		}else if(scaleSize<SCALSIZE_LIMIT.min){
			//超出范围不做响应
			return -1;
		}

		this.scaleSize=scaleSize;
		this.pxPerUnitWidth=SCALESIZE[scaleSize].pxPerUnitWidth; //单位长的像素距离
		this.pxPerUnitHeight=SCALESIZE[scaleSize].pxPerUnitHeight;//单位高的像素距离

		return 0;
	}
   
    /*设置坐标系视图中心的像素坐标点
	 x-x坐标
	 y-y坐标
    */
    setViewCenter(x,y){
    	//如果原始原点不存在则设置
    	if(!this.originCenterPX){
    		this.originCenterPX=new PXPosition(x,y);
    	}

    	this.centerPx.x=x;
    	this.centerPx.y=y;
    }

    /*设置坐标系视图中心的坐标位置
	x-x坐标
	y-y坐标
    */
    setCoordCenter(x,y){
    	//将坐标转换为像素坐标
    	let pxPosition=new PXPosition(
    		x*this.pxPerUnitWidth+this.pxPerUnitWidth/2,y*this.pxPerUnitHeight+this.pxPerUnitHeight/2
    		);
    	this.setViewCenter(pxPosition.x,pxPosition.y)

    }

   /* 
   	获取坐标系的中心坐标（非像素坐标）
   */
    getCoordCenter(){
    	let coordX=(this.centerPx.x-this.pxPerUnitWidth/2)/this.pxPerUnitWidth;
    	let coordY=(this.centerPx.y-this.pxPerUnitHeight/2)/this.pxPerUnitHeight;
    	return [coordX,coordY]
    }

   /* 移动坐标系视图中心像素坐标点
	xdistance-x 轴移动距离
	ydistance-y轴移动距离
   */
    moveViewCenter(xdistance,ydistance){
    	this.centerPx.x+=xdistance;
    	this.centerPx.y+=ydistance;
    }



    /*设置锚点
	xCoordPx-x 在当前坐标系内的像素横坐标
	yCoordPx-y 在当前坐标系内的像素横坐标
    */
    setAnchor(xCoordPx,yCoordPx){

	}


	

	translatePxCoordinate(position){
		this.coordinateSystem.translatePxPosition();
	}


	/*对canvas视图坐标，转换为坐标系坐标。不计算便宜量，用于绘图逻辑视图坐标
	viewPxPosition-像素坐标
	return-viewPxPosition像素坐标
	*/
	translateFromCanvasXY(viewPxPosition,canvas){
		return this.coordinateSystem.translateFromCanvasXY(viewPxPosition,canvas);
	}

	/*对坐标系视图坐标，转化为canvas坐标。不计算偏移量，用于在canva绘图
	viewPxPosition-像素坐标
	return-viewPxPosition像素坐标
	*/
	translateToCanvasXY(viewPxPosition,canvas){
		return this.coordinateSystem.translateToCanvasXY(viewPxPosition,canvas);
	}


	/*对canvas移动距离转换为坐标系内的移动距离（主要转换方向）
	xyCanvasDistance-[xdistance,ydistance]  canvas的像素移动距离
	return-[xdistance,ydistance] 坐标系的移动距离
	*/
	translateDistanceFromCanvasXY(xyCanvasDistance){
		return this.coordinateSystem.translateDistanceFromCanvasXY(xyCanvasDistance);
	}


	/*获取像素区域所显示坐标轴范围
	startViewPxPosition- 起始视图区的像素位置（不含坐标系偏移）
	endViewPxPosition-结束视图区的像素位置（不含坐标系偏移） 
	return-坐标范围[startX,startY,endX,endY]
	*/
	getCoordinateExtendByViewExtend(startViewPxPosition,endViewPxPosition){
		let offsetXY=this.getSystemPxOffset();
		//将像素位置转换为，坐标系像素位置（含偏移量）
		let startPxPosition=new PXPosition(startViewPxPosition.x+offsetXY[0],startViewPxPosition.y+offsetXY[1]);
		let endPxPosition=new PXPosition(endViewPxPosition.x+offsetXY[0],endViewPxPosition.y+offsetXY[1])

		//计算像素点所在的坐标区域
		let minX=startPxPosition.x;
		let minY=startPxPosition.y;
		let maxX=endPxPosition.x;
		let maxY=endPxPosition.y

		if(minX>maxX){
			minX=endPxPosition.x;
			maxX=startPxPosition.x;
		}

		if(minY>maxY){
			minY=endPxPosition.y;
			maxY=startPxPosition.y
		}



		let pxPositionExtend=[]
		

		let x1=Math.floor( minX / this.pxPerUnitWidth);
		let y1=Math.floor( minY / this.pxPerUnitHeight);

		let x2=Math.floor(maxX / this.pxPerUnitWidth);
		let y2=Math.floor(maxY / this.pxPerUnitHeight);

		

		return [x1,y1,x2,y2]
	}


	/*根据坐标获取像素坐标（含偏移量）

	*/
	getPxPositionFromCoordinate(coordX,coordY){
		return new PXPosition(coordX*this.pxPerUnitWidth,coordY*this.pxPerUnitHeight)
	}

	/*获取视图像素位置的坐标-不带小数

	*/
	getCoordinateFromViewPxPosition(viewPxPosition){
		//计算pxPosition
		let systemOffset=  this.getSystemPxOffset();
		let pxPosition=new PXPosition(viewPxPosition.x+systemOffset[0],viewPxPosition.y+systemOffset[1])
		//计算坐标

		let coordX=Math.floor(pxPosition.x/this.pxPerUnitWidth) ;
		let coordY=Math.floor(pxPosition.y/this.pxPerUnitHeight) ;
		return new CoordPosition(coordX,coordY);
	}

	/*20210109
	获取坐标抽像素坐标-带小数
	viewPxPosition-视图像素位置（canvas）
	其中混杂了对坐标系的转换--不明了。应该通过统一转换
	*/
	getPxPositionFromViewPxPosition(viewPxPosition){
		//获取视图中心点的像素坐标
		let viewCenter=this.getViewCenter();
		//获取指定视图点到视图中心点的像素偏移量
		let coord=viewPxPosition;
		let width=this.viewSize[0];
		let height=this.viewSize[1];
		let systemViewPosition=this.coordinateSystem.translateCoordToDifferentSystem(coord,width,height,ORIGIN_MODE.LT,ORIGIN_MODE.LB);
		
		let xPxOffset= systemViewPosition.x-this.viewSize[0]/2;
		let yPxOffset= systemViewPosition.y-this.viewSize[1]/2;

		//计算指定点的像素坐标
		let pxPosition=new PXPosition(viewCenter.x+xPxOffset,viewCenter.y+yPxOffset)
		return pxPosition;
	}
	/*20210109
	获取坐标抽 坐标-不带小数
	viewPxPosition-视图像素位置（canvas）
	其中混杂了对坐标系的转换--不明了。应该通过统一转换
	*/
	getCoordFromViewPxPosition(viewPxPosition){
		let that=this;
		let pxPosition=this.getPxPositionFromViewPxPosition(viewPxPosition);
		return new CoordPosition(Math.floor(pxPosition.x/that.pxPerUnitWidth),Math.floor(pxPosition.y/that.pxPerUnitHeight));
	}

	/*获取视图像素位置的坐标-带小数
	*/
	getCoordinateFromViewPxPositionExact(viewPxPosition){
		//计算pxPosition
		let systemOffset=  this.getSystemPxOffset();
		let pxPosition=new PXPosition(viewPxPosition.x+systemOffset[0],viewPxPosition.y+systemOffset[1])
		//计算坐标

		let coordX=pxPosition.x/this.pxPerUnitWidth ;
		let coordY=pxPosition.y/this.pxPerUnitHeight ;
		return new CoordPosition(coordX,coordY);
	}


	/*获取坐标在当前视图中的像素坐标区域，
	如果当前坐标已超出可视范围，则返回 [ ViewPXPosition(-1,-1),ViewPXPosition(-1,-1)]
	coordX-x坐标
	coordY-y坐标
	return-
	*/
	getViewPxPositionExtendFromCoordinate(coordX,coordY,canvas){


		let pxPosition=this.getPxPositionFromCoordinate(coordX,coordY);
		let systemOffset=  this.getSystemPxOffset();
		let viewPxPosition=new ViewPXPosition(pxPosition.x-systemOffset[0],pxPosition.y-systemOffset[1])

		//判断该点是否超出可视区(可视区，因为是以右下角为起点，所以最小值因>-pxPerUnitWidth,高也是如此)
		let outOfView=0;
		if(viewPxPosition.x+this.pxPerUnitWidth<0 ||viewPxPosition.x>canvas.width){
			outOfView=1 ;

		}

		if(viewPxPosition.y+this.pxPerUnitHeight<0||viewPxPosition.y>canvas.height){
			outOfView=1 ;
		}

		if(outOfView==1){
			return [ new ViewPXPosition(-1,-1),new ViewPXPosition(-1,-1)]
		}else{
			/*this.pxPerUnitWidth=16; //单位长的像素距离
			this.pxPerUnitHeight=16;//单位高的像素距离*/
			let startViewPxPosition=viewPxPosition;
			let endViewPxPosition=new ViewPXPosition(startViewPxPosition.x+this.pxPerUnitWidth,startViewPxPosition.y+this.pxPerUnitHeight);
			
			return [startViewPxPosition,endViewPxPosition]
		}
		

		
	}


	/*获取坐标系像素偏移量
	return-[xPxOffset,yPxOffset]
	*/
	getSystemPxOffset(){
		let origin=this.originCenterPX; 
		let now=this.centerPx;
		return [now.x-origin.x,now.y-origin.y];
	}

	/*获取当前视图中心的坐标系像素坐标-含偏移量

	*/
	getViewCenter(){
		return this.centerPx;
	}


	/*获取当前canvas视图中包含的坐标范围
	*/
	getViewCoordExtend(canvas){
		
		let startViewPxPosition=new ViewPXPosition(0,0);
		let endViewPxPosition=new ViewPXPosition(canvas.width,canvas.height);
		return this.getCoordinateExtendByViewExtend(startViewPxPosition,endViewPxPosition)
	}

	/*获取 区域 相对于坐标系 的起点，即由相对坐标系 minX minY组成的点
	*/
	getStartPointFromCanvasViewPXPositionExtend(viewPxPosition1,viewPxPosition2){
		return this.coordinateSystem.getStartPointFromCanvasViewPXPositionExtend(viewPxPosition1,viewPxPosition2);
	}
}

/*缩放级别对应的尺寸*/
let SCALESIZE={
	1:{"pxPerUnitWidth":8,"pxPerUnitHeight":8},
	2:{"pxPerUnitWidth":16,"pxPerUnitHeight":16},
	3:{"pxPerUnitWidth":24,"pxPerUnitHeight":24},
	4:{"pxPerUnitWidth":32,"pxPerUnitHeight":32},
	5:{"pxPerUnitWidth":40,"pxPerUnitHeight":40},
	6:{"pxPerUnitWidth":48,"pxPerUnitHeight":48},
	7:{"pxPerUnitWidth":56,"pxPerUnitHeight":56},
	8:{"pxPerUnitWidth":64,"pxPerUnitHeight":64},
	9:{"pxPerUnitWidth":72,"pxPerUnitHeight":72},
	10:{"pxPerUnitWidth":80,"pxPerUnitHeight":80}
	
}

let SCALSIZE_LIMIT={
	min: 1,
	max: 10
}

export {CoordinateSystem};


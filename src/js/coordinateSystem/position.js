

//像素位置，从坐标系原点计算
class PXPosition{

	constructor(x,y){
		this.x=x;
		this.y=y;
	}

}

//坐标位置,从坐标系原点计算
class CoordPosition{
	constructor(x,y){
		this.x=x;
		this.y=y;
	}
}


//视图区域的像素位置，从视图原点计算
class ViewPXPosition{

	constructor(x,y){
		this.x=x;
		this.y=y;
	}

}

export {ViewPXPosition,PXPosition,CoordPosition};

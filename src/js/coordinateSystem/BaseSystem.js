
class BaseSystem{


	constructor(IBase) {

		//定义单位距离的像素数量
		this.pxPerUnit=16;  
       
    }

    getExtenCoordinate(coo){



    }

    /*转换不同坐标系的坐标
	coord           -来源坐标系坐标
	width           -视图宽
	height          -视图高
	fromOriginMode  -来源坐标系模式-ORIGIN_MODE
	toOriginMode    -目标坐标系模式-ORIGIN_MODE
    */
    translateCoordToDifferentSystem(coord,width,height,fromOriginMode,toOriginMode){
    	let name=fromOriginMode+toOriginMode;
    	if(name=="LTLB"||name=="LBLT"){
    		//y-相反
    		coord.y=height-coord.y;
    	}else if(name=="LTRT"){
    		//x-相反
    		coord.x=width-coord.x;
    	}else if(name=="LTRB"){
    		//x-y 相反
    		coord.x=width-coord.x;
    		coord.y=height-coord.y;

    	}else if(name=="LBRT"){
    		//x-y 相反
    		coord.x=width-coord.x;
    		coord.y=height-coord.y;
    	}else if(name=="LBRB"){
    		//x-相反
    		coord.x=width-coord.x;
    	}else if(name=="RTRB"){
    		//y-相反
    		coord.y=height-coord.y;
    	}
    	return coord;
    	
    }

}

let ORIGIN_MODE={
	"LT":"LT",
	"LB":"LB",
	"RT":"RT",
	"RB":"RB",

}
export {BaseSystem,ORIGIN_MODE};
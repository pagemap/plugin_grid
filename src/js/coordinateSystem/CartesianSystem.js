
import {BaseSystem} from './BaseSystem.js'
import {ViewPXPosition} from './position.js'

class CartesianSystem extends BaseSystem{

	constructor() {
		super();
       
    }

    /*对canvas视图坐标，转换为坐标系坐标。不计算偏移量，用于绘图逻辑视图坐标
	pxPosition-像素坐标
	*/
	translateFromCanvasXY(pxPosition,canvas){
		return new ViewPXPosition(pxPosition.x,canvas.clientHeight-pxPosition.y);
	}

	/*对坐标系视图坐标，转化为canvas坐标。不计算偏移量，用于在canva绘图
	pxPosition-像素坐标
	*/
	translateToCanvasXY(pxPosition,canvas){
		return new ViewPXPosition(pxPosition.x,canvas.clientHeight-pxPosition.y);
	}

	/*对canvas移动距离转换为坐标系内的移动距离（主要转换方向）
	xyCanvasDistance-[xdistance,ydistance]  canvas的像素移动距离
	return-[xdistance,ydistance] 坐标系的移动距离
	*/
	translateDistanceFromCanvasXY(xyCanvasDistance){
		//将x轴距离变换方向即可
		return [0-xyCanvasDistance[0],xyCanvasDistance[1]];
	}


	getStartPointFromCanvasViewPXPositionExtend(viewPxPosition1,viewPxPosition2){
		//获取4角顶点
		let x1=viewPxPosition1.x<=viewPxPosition2.x?viewPxPosition1.x:viewPxPosition2.x;
		let y1=viewPxPosition1.y<=viewPxPosition2.y?viewPxPosition1.y:viewPxPosition2.y;
		
		let x2=viewPxPosition1.x>=viewPxPosition2.x?viewPxPosition1.x:viewPxPosition2.x;
		let y2=viewPxPosition1.y>=viewPxPosition2.y?viewPxPosition1.y:viewPxPosition2.y;

		/*在笛卡尔坐标系中因选取左下角的点作为起点*/

		return new ViewPXPosition(x1,y1);
	}
}

export {CartesianSystem};
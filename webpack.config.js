let path = require('path');
let webpack=require('webpack');

module.exports = {

		entry: './src/js/MFGrid.js',
		output:{
				path:path.join(__dirname,'dist'),
				filename:"mfgrid.js",

				library: {
				  root: "YL",
				  amd: "YL",
				  commonjs: "YL"
				},
				libraryTarget: "umd"
		},
		module:{
			rules:[
			
			
			]
		},
		
		optimization:{
			minimize: false, // <---- 禁用 uglify.
			// minimizer: [new UglifyJsPlugin()] 使用自定义压缩工具
		},

		
		
		
	
}	